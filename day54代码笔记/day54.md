今日内容总结

django下载安装

pip install django==1.11.9

配置环境变量:scripts那个路径配置到环境变量里面

创建项目

cmd  : django-admin startproject 项目名称(mydjango)

cd mydjango

创建app: django-admin startapp 应用名(app01)      python manage.py startapp 应用名

启动项目:python manage.py runserver 127.0.0.1:8001  

注册app,settings配置文件中:

```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app01.apps.App01Config',
    # 'app02.apps.App02Config',
    'app02',
]
```

pycharm创建启动项目(...)

第一步:

​	urls.py里面:

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^index/', views.index),

]
```

第二步:

​	views.py视图函数,写咱的逻辑

```python
def index(request):
    return HttpResponse('app01')  
```



第三步:

​	templates 文件夹里面创建一个html文件 :index.html

```
def index(request):
	return render(request,'index.html',{'name':'chao'})
index.html
	{{ name }}
```



返回内容的方法:

```
Httpresponse('xx') 返回文本字符串
render(request,'index.html',{'name':'chao'})  返回模板文件的,html
```



url高级用法

```
url(r'^index/(?P<year>\d+)/(\d+)/', views.index,{'foo':'xxxxx'})  
有名分组:

include分发
在应用文件夹里面创建一个自己的urls路由文件
在总的路由文件里面写上下面的内容:
url(r'^app01/',include('app01.urls'))


```





 













