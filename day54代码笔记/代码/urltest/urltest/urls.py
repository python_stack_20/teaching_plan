"""urltest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from app01 import views
urlpatterns = [
    # url(r'^admin/', admin.site.urls),

    url(r'^articles/$', views.articles),
    # url(r'^articles/2003/$', views.articles_2003),
    # url(r'^articles/(\d{4})/$', views.articles_year),
    # url(r'^articles/(\d{4})/(\d{1,2})/$', views.articles_year_month),
    # url(r'^articles/2004/$', views.articles_2004),
    # url(r'^articles/(?P<year>\d{4})/(?P<month>\d{1,2})/$', views.articles_year_month),
    # url(r'^articles/(?P<year>\d{4})/$', views.articles2),

    # include
    #app01应用的
    url(r'^app01/', include('app01.urls')),

    #app02应用的
    url(r'^app02/', include('app02.urls')),


]
