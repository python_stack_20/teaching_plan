2019.7.12
1.CBV - FBV

2.从头到尾复习 Flask
Flask优势:
	轻量级框架,扩展性,三方组件全
Flask劣势:
	太轻了,只有个Session,组件是第三方的,稳定性相对较差

pip3 install Flask
Jinja2 模板语言
Flask  源码
MarkupSafe 处理标签语言 render_template
Werkzeug|UWSGI WSGI 承载Flask程序	 德语工具的意思

1.启动Flask
from flask import Flask
app = Flask(__name__)
app.run("0.0.0.0",9527)

2.路由视图
@app.route("/index",methods=["GET","POST"],endpoint="index")
def index():
	return "Hello"
	
methods 当前路由允许的请求方式
endpoint Mapping -> "/index":{"index":index}

3.FlaskResponse
1.render_template(.html) 
2.redirect("/login")
3."" 

4.jsonify  # content-type:application/json
- Flask 1.1.1 新特性 可以直接返回dict类型 - 本质上就是在jsonify({a:1})
5.send_file # 打开并返回文件内容自动识别文件类型 content-type:文件类型

4.FlaskRequest
from flask import request #公共变量 LocalProxy对象
request.POST.get() == request.form.get() -> .to_dict()
request.GET.get() == request.args.get() -> .to_dict()

request.json	# 请求头中带content-type:application/json
request.data	# content-type无法被识别 或者是 没有 Form

request.files.get() 获取文件
request.method 
request.headers 
request.cookie 
request.path
request.url
request.host
request.host_url

5.Session
from flask import session
app.secret_key = "$%^&*("

def vf():
	session["name"] = 1
	
存储在Cookies中
序列化 - {name:1} -> secret_key + 时间 + 签名 -> 生成一个字符串 -> SESSION_COOKIE_NAME : "生成一个字符串"

反列化 - SESSION_COOKIE_NAME : "生成一个字符串" -> secret_key + 时间 + 签名 -> {name:1}


6.Flask配置
1.初始化 - app = Flask(__name__)
template_folder 	
static_folder		
staric_url_path		

2.Config Flask对象配置
Flask.default_config 获取默认配置
DEBUG
TESTING
SESSION_COOKIE_NAME
SECRET_KEY

class DebugSetting(object):
	DEBUG = True
	SECRET_KEY = "#$%^&U*I("
	SESSION_COOKIE_NAME = "I am Not Session"

Flask.config.from_obj(DebugSetting)
Flask.config["DEBUG"] = True


7.蓝图 ("bpname",__name__)
from flask import Blueprint
不能被run的Flask实例 没有Config
蓝图作用 - app隔离,URL管理
#Alexander.DSB.Li
#AlexDSB
userBP = Blueprint("userBP",__name__,url_prefix="/user")
@userBP.route("/userBP")
def user_bp():
	return ""
	
思考: 如何在蓝图中使用 CBV 结构


8.特殊装饰器
1.before_request # 请求进入视图函数之前
2.after_request	# 结束视图函数之后,响应客户端之前
be+af 实现中间件:
正常周期: be1 - be2 - vf - af2 - af1
异常周期: be1 - af2 - af1

3.errorhandler(HTTP_ERROR_CODE 5xx 4xx) # 重定义错误信息

9.CBV
from flask import views

class Login(views.MethodView):
	methods = []
	
	def get(self): # 满足 HTTP 协议 8 请求
		pass
	
	def post(self):
		pass
		
Flask.add_url_rule(rule="/login",endpoint=None,view_func=Login.as_view(name="login"))

	
tu = (1,2,3,4)
tu_l = list(tu)
tu_l.append(5)
tu = tuple(tu_l)



Flask-Session
Flask-CORS
Flask-WTF
Flask-SQLAlchemy ORM

from flask_session import Session

app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = Redis(host="127.0.0.1",port=6379,db=6)

Session(app) # Flask-Session 读取config配置 改写 APP 中的 session_interface

from flask import session 
更改 session 的执行属性

所有的Flask第三方组件 都是需要 app.config 不但要吸取配置项还会更改或者增加配置项




