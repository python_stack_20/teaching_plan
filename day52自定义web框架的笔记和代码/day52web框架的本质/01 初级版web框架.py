import socket
from threading import Thread
server = socket.socket()
server.bind(('0.0.0.0',8001))
server.listen()
def communication(conn):
    #接受请求数据
    client_msg = conn.recv(1024).decode('utf-8')
    #组合响应协议的消息格式,然后发送响应信息
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    #打开index.html文件,返回给前端
    with open('01 index.html', 'rb') as f:  #没有引入文件形式的html页面
    # with open('01 index2.html', 'rb') as f:   #引入了文件形式的html页面
        data = f.read()
    conn.send(data)
    conn.close()


while 1:
    #接受连接
    conn, add = server.accept()
    #开启线性并发
    t = Thread(target=communication,args=(conn,))
    t.start()























