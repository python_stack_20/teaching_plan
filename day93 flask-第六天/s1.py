import json

from bson import ObjectId
from pymongo import MongoClient
# from pymongo.cursor import Cursor

m_client = MongoClient("127.0.0.1",27017)

mdb = m_client["S20"]

# 增
# 增加数据时 res.inserted_id or inserted_ids
# 都是 ObjectId不是字符串
# res = mdb.user.insert_one({"name":"DragonFire"})
# print(res.inserted_id)  # 5d2ed8f865d6b8f1c494ff78
# res = mdb.user.insert_many([{"name":"小黑"},{"name":"小二B"}])
# print(res.inserted_ids)

# 查:
# res = list(mdb.user.find({}))
# print(res)
# res_list = []
#
# for user in res:
#     user["_id"] = str(user.get("_id"))
#     res_list.append(user)
#
# res_json = json.dumps(res_list)
# print(res_json)


# res = list(mdb.user.find({}))
# print(res)
#
# for index,user in enumerate(res):
#     res[index]["_id"] = str(user.get("_id"))
#
# res_json = json.dumps(res)
# print(res_json)


# res = mdb.user.find_one({"name":"小黑"})
# res = mdb.user.find_one({"_id":ObjectId("5d2ed96e38887b85450ed6d8")})
# res["_id"] = str(res.get("_id"))
# print(res,str(res.get("_id")))
# res_json = json.dumps(res)
# print(res_json)


# 修改数据
# res = mdb.user.update_one({"name":"小二B"},{"$set":{"name":"A_C"}})
# print(res.modified_count)
#
# user = mdb.user.find_one({"name":"A_C"})
# user["gender"] = 1
# user["age"] = 99
# user["name"] = "小二A_C"
# user["hobby"] = ["choyan","hj","xzt"]
#
# res = mdb.user.update_one({"name":"A_C"},{"$set":user})

# user = mdb.user.find_one({"name":"小二A_C"})
# user["hobby"].append("洗头")
# user["hobby"].remove("hj")
#
# mdb.user.update_one({"name":"小二A_C"},{"$set":{"hobby":user.get("hobby")}})


# 删除
# res = mdb.user.delete_one({})
# print(res.deleted_count)

import pymongo
# 排序 + skip + limit
# res = list(mdb.stu.find().sort("age",pymongo.DESCENDING))
# res = list(mdb.stu.find().skip(2))
# res = list(mdb.stu.find().limit(2))
# for s in res:
#     print(s)

# 分页
# res = list(mdb.stu.find().sort("age",pymongo.ASCENDING).limit(2).skip(4))
# print(res)