from django import template
from django.utils.safestring import mark_safe
register = template.Library() #名字必须是register

@register.filter
def xxx(v1,v2):
# def xxx(v1):
    '''
    最多有两个参数
    :param v1:  变量值
    :param v2:  过滤器参数值,
    :return:
    '''
    # v2 = 30
    print(v1,v2) #100 20
    return v1*v2  #必须return值


# 自定义标签
@register.simple_tag
def sss(v1,v2,v3):  #可以传多个参数

    print(v1,v2,v3)

    # return v1 - v2 - v3 #必须return值
    s = '<a href=''>百度</a>'
    # return s #必须return值
    return mark_safe(s) #必须return值

@register.inclusion_tag('result.html')
def aaa():
    data=[11,22,33]
    return {'data':data}
