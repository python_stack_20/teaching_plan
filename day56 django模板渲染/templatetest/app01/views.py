from django.shortcuts import render,HttpResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect
# Create your views here.

class MyData:
    def __init__(self,name):
        self.name=name

    def age(self):
        return '18'
    # def age2(self,num):
    #     return '18'

def index(request):
    # a = 20
    # b = {'k1':'v1'}
    # c = [11,22]
    # m = MyData('一熊')
    # l1 = [{'sex':'女'},22]
    # s = 'XIAOHEI'
    # # return render(request,'index.html',{'name':'chao','m':m,'b':b,'c':c,'a':a,'l1':l1})
    # ss = 'B哥'
    # num = 12345
    # import datetime
    # date = datetime.datetime.now()
    #
    # atag = "<a href='http://www.baidu.com'>百度</a>"
    #
    # sss = 'hello beautiful girl yue ma!'
    #
    # l2 = ['hello','girl']
    #
    # print(locals())
    # return render(request,'index.html',locals())

    return HttpResponse('<a href="xx">百度</a>')


def base(request):
    l1 = [11,22,33]
    name_list = ['B哥','H哥','P哥','L哥','L2哥','J哥']

    name_dict = {'B哥':'王增福','H哥':'天鸿','P哥':'徐鹏'}
    num = 101

    return render(request,'base.html',{'name_list':name_list,'name_dict':name_dict,'l1':l1,'num':num})


@csrf_protect  #强制csrf认证,即便是全局没有配置csrf认证
def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('username')
        print(request.POST) #<QueryDict: {'csrfmiddlewaretoken': ['OhZYiA1NHjh6ywLldzLOzy4N2OXOmHZH4tIlZvTe5ll0p6OSNUhuEgu3aOalUZoo'], 'username': ['chao']}>
        print(username)
        return HttpResponse('ok')


def home(request):

    return render(request,'home.html')

def menu1(request):

    return render(request,'menu1.html')


def menu2(request):
    return render(request, 'menu2.html')


def menu3(request):
    return render(request, 'menu3.html')




def home1(request):

    return render(request,'home1.html')

def menu11(request):

    return render(request,'menu11.html')


def menu12(request):
    return render(request, 'menu12.html')


def menu13(request):
    return render(request, 'menu13.html')


def title(request):
    return render(request,'title.html')


#组件
def sidebar(request):

    return render(request,'sidebar.html')


def tags(request):
    num = 100
    return render(request,'tags.html',{'num':num})
