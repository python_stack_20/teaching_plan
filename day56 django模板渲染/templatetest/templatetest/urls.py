"""templatetest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app01 import views
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^index/', views.index),
    url(r'^base/', views.base),
    url(r'^login/', views.login),
    url(r'^home/', views.home),
    url(r'^home1/', views.home1),
    url(r'^menu1/', views.menu1),
    url(r'^menu2/', views.menu2),
    url(r'^menu3/', views.menu3),


    url(r'^menu11/', views.menu11),
    url(r'^menu12/', views.menu12),
    url(r'^menu13/', views.menu13),
    #组件
    url(r'^title/', views.title),
    url(r'^sidebar/', views.sidebar),
    #自定义 过滤器和标签
    url(r'^tags/', views.tags),




]
