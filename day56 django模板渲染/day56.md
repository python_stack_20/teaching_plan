# 昨日内容回顾

路由系统url

```
    无名分组
    url(r'^articles/(\d{4})/(\d{1,2})/$', views.articles_year_month)
    def articles_year_month(request,n1,n2):
    	pass
    有名分组
	url(r'^articles/(?P<year>\d{4})/(?P<month>\d{1,2})/$', views.articles_year_month),
    def articles_year_month(request,year,month):
    	pass	
	
	默认参数
	    def articles_year_month(request,month,year='2012'):
    	pass
	额外参数
	url(r'^articles/(?P<year>\d{4})/(?P<month>\d{1,2})/$', views.articles_year_month,{'foo':'xxx'})
	
	def articles_year_month(request,month,foo,year='2012'):pass
	
	
```



路由分发include

```
项目的路由urls.py里面
app01,app02
1 导入include
2 app文件夹中创建每个app的urls.py文件
3 url(r'^app01/', include('app01.urls'))

url(r'^$', views.home), #匹配首页
```





今日内容

视图

CBV 和 FBV

```
FBV:def index(request):pass

CBV:
from django.views import View
class LoginView(View): #继承View
	
	#重写dispatch方法的写法
	def dispatch(self,request,*args,**kwargs)
		#执行父类的dispatch方法
		ret = super().dispatch(request,*args,**kwargs)
		return ret  #别忘了return值
		
    def get(self,request): #根据用户的请求方法,找到对应的方法

        return render(request,'login.html')

    def post(self,request):
        print(request.POST)
        uname = request.POST.get('username')
        pwd = request.POST.get('password')
        if uname == 'chao' and pwd == '123':
            return render(request, 'home.html')
        return HttpResponse('用户名或者密码错误')
        
urls.py
 	url(r'^login/', views.LoginView.as_view()), 1.as_view()--view,2用户请求到了 view,3view里面执行了dispatch,反射找到请求方法对应的自己视图的方法
 	
 	
FBV装饰器
#装饰器
def xxx(func1):
    def inner(request,*args,**kwargs):
        print('xx1')
        ret = func1(request,*args,**kwargs)
        print('xx2')
        return ret
    return inner
@xxx
def index(request):pass

CBV
from django.utils.decorators import method_decorator

方式三:
@method_decorator(xxx,name='get')
class IndexView(View):
	方式一:
	@method_decorator(xxx)
	def dispatch(...):....
	
	方式二:
	@method_decorator(xxx)
	def get(self,request):pass
	def post(self,request):pass

```

request  请求对象

```
request.body:得到用户提交数据的原始数据类型 b'username=chao&passowrd=123'
request.GET
request.POST
request.method 获取请求方法
request.path_info  获取用户请求路径
request.get_full_path() 获取除域名之外的全路径,包括请求参数(query_params)
request.META  获取请求头的元信息

```

response 响应对象

```
三个方法
HttpResponse('xxx')  返回一个字符串
render(request,'index.html',{'name':'chao'})  index.html  {{ name }}--chao
redirect('/login/') -- 给浏览器发送了一个重定向的请求,浏览器拿到你要重定向的url,然后自动发送了一个ip+端口+路径/login/,的一个请求,后端在配置对应的url('^login/',views.login)

```



模板语言

```
{{ 变量 }} {% 逻辑 %}
```

万能的句点号,name.xxx.xxx,   li.0  d1.key  

内置的过滤器

```
{{ 变量|过滤器名称:'过滤器需要的参数' }}
safe
date   时间对象|date:'Y-m-d H:i:s'
join
cut
truncatechars
truncatewords
length
slice
default
lower  upper
filesizeformat

```





# 今日内容

内置标签 {%  %}

```
for循环,循环一个列表
<ul>
    {% for name in name_list reversed %}
        <li>{{ forloop.counter }}  {{ name }}</li>
    {% endfor %}
</ul>
循环一个字典
<ol>
    {% for key,value in name_dict.items %}
        <li> {{ forloop.counter0 }} {{ key }}  ---  {{ value }} </li>
    {% endfor %}

</ol>
不支持break等操作
if判断 结合过滤器的写法
    {% if num|xxx:20 > 2000 %}
    <h2>大于2千</h2>
    {% else %}
        <h2>小于等于2千</h2>
    {% endif %}


```



csrf_token

```python
防御跨站请求伪造
	我们以post方式提交表单的时候，会报错，还记得我们在settings里面的中间件配置里面把一个csrf的防御机制给注销了啊，本身不应该注销的，而是应该学会怎么使用它，并且不让自己的操作被forbiden，通过这个东西就能搞定。
	这个标签用于跨站请求伪造保护，
	在页面的form表单里面（注意是在form表单里面）任何位置写上{% csrf_token %}，这个东西模板渲染的时候替换成了<input type="hidden" name="csrfmiddlewaretoken" value="8J4z1wiUEXt0gJSN59dLMnktrXFW0hv7m4d40Mtl37D7vJZfrxLir9L3jSTDjtG8">，隐藏的，这个标签的值是个随机字符串，提交的时候，这个东西也被提交了，首先这个东西是我们后端渲染的时候给页面加上的，那么当你通过我给你的form表单提交数据的时候，你带着这个内容我就认识你，不带着，我就禁止你，因为后台我们django也存着这个东西，和你这个值相同的一个值，可以做对应验证是不是我给你的token，存储这个值的东西我们后面再学，你先知道一下就行了，就像一个我们后台给这个用户的一个通行证，如果你用户没有按照我给你的这个正常的页面来post提交表单数据，或者说你没有先去请求我这个登陆页面，而是直接模拟请求来提交数据，那么我就能知道，你这个请求是非法的，反爬虫或者恶意攻击我的网站，以后将中间件的时候我们在细说这个东西，但是现在你要明白怎么回事，明白为什么django会加这一套防御。

html
<form action="/login/" method="post">
    {% csrf_token %} form表单里面的任意位置
    用户名:<input type="text" name="username">
    <input type="submit">

</form>

'django.middleware.csrf.CsrfViewMiddleware', 全局的配置
通过它还有一种方式
装饰器的形式

#放行csrf认证,即便是全局配置了csrf认证
@csrf_exempt  
def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('username')
        print(request.POST) #<QueryDict: {'csrfmiddlewaretoken': ['OhZYiA1NHjh6ywLldzLOzy4N2OXOmHZH4tIlZvTe5ll0p6OSNUhuEgu3aOalUZoo'], 'username': ['chao']}>
        print(username)
        return HttpResponse('ok')
        
还有一种强制保护
@csrf_protect  #强制csrf认证,即便是全局没有配置csrf认证
def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('username')
        print(request.POST) #<QueryDict: {'csrfmiddlewaretoken': ['OhZYiA1NHjh6ywLldzLOzy4N2OXOmHZH4tIlZvTe5ll0p6OSNUhuEgu3aOalUZoo'], 'username': ['chao']}>
        print(username)
        return HttpResponse('ok')

```



模板继承

```
1创建一个模板.html文件,

2{% extends '模板.html' %}

3 模板.html  {% block content %} xxxxxxx{% endblock %} 还可以指定名字{% endblock content%}

4 继承模板的文件里面 {% block content %} 自己html里面的内容{% endblock %}

5 保留模板内容的写法 {% block content %} {{ block.super }}自己html里面的内容{% endblock %}
```



组件:

```
将一个完整功能模块,放到一个hmtl文件中,使用这个模块的其他页面,直接在页面中引入即可,

引入方式{% include '模块.html' %} ,任意位置引入
```

组件和插件的简单区别

```
组件是提供某一完整功能的模块，如：编辑器组件，QQ空间提供的关注组件 等。

而插件更倾向封闭某一功能方法的函数。

这两者的区别在 Javascript 里区别很小，组件这个名词用得不多，一般统称插件。
```

自定义标签和过滤器

步骤:

```
1 app应用文件夹中创建一个叫做templatetags的文件夹
2 创建一个py文件,例如:mytag.py
3 mytag.py,
	from django import template
	register = template.Library()  #register变量必须是这个名字
	过滤器:
	@register.filter
	def xxx(): 参数最多两个
    	return 'xxx'
	标签:
	@register.simple_tag
	def sss(): 参数可以多个
    	return 'xxx'
	
	标签:
	@register.inclusion_tag('html文件')
	def aaa(): 参数可以多个
    	return {'xx':'xxxxx'}
    	
html文件中的使用
<h1>
{#    {{ num|xxx:'11,23,33'}}#}
    {{ num|xxx:20 }}
    {% if num|xxx:20 > 2000 %}
    <h2>大于2千</h2>
    {% else %}
        <h2>小于等于2千</h2>
    {% endif %}

</h1>

<h1>
    {% sss num 10 15 %}
</h1>

<h1>
    {% aaa  %}
</h1>

```



静态文件配置



























