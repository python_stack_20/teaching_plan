# 事件冒泡

点击子标签,会一级一级的网上触发父级祖父级等等相同点击事件

阻止事件冒泡

```javascript
    $('.cc').click(function () {
        alert('我是父级标签');
    });
    $('.c1').click(function (e) {
        alert('我是子标签');
       // $('.cc').off('click');
       //  return false;  方式1
        e.stopPropagation();  方式2
    })
```



```
<tr><td><input type="checkbox"></td><td>王涛(回首掏)</td><td>走位,看不见,难受</td><td><button class="del">删除</button></td></tr>
```





# 今日内容回顾

事件冒泡



阻止事件冒泡

```javascript
return false

e.stopPropagation()
```

事件委托

```
$(祖先标签).on('click','后代标签选择器',function(){})

```

页面载入

```
$(function(){})
$(document).ready(function(){})
```

动画了解

each循环

```
$.each(数组/{},function(k,v){}) k索引 v值
$('div').each(function(k,v){}) k索引,v是dom标签对象

return false;结束循环
return;结束本次循环
```

data

```
$('div').data('name','值')
$('div').data('name')
$('div').removeData('name')

```

插件

```
$.extend({func1:function(){}})
$.fn.extend({})
```





















