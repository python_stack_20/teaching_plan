from flask import Flask, render_template, redirect, jsonify, send_file

app = Flask(__name__)

@app.route("/")
def home():
    return "Hello World!"

@app.route("/index")
def index():
    return render_template("index.html")

@app.route("/reback")
def reback():
    return redirect("/index")


@app.route("/json")
def my_jsonify():
    # return jsonify({"a":1})
    return {"k":"v"}

@app.route("/my_file")
def my_file():
    return send_file("1.rar")

app.run()