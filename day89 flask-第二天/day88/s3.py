from flask import Flask,render_template,Markup

STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'}

STUDENT_LIST = [
    {'name': 'Old', 'age': 38, 'gender': '中'},
    {'name': 'Boy', 'age': 73, 'gender': '男'},
    {'name': 'EDU', 'age': 84, 'gender': '女'}
]

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}


app = Flask(__name__)
app.debug = True
# app.config["DEBUG"] = True

@app.template_global()
def ab(a,b):
    return a+b


@app.route("/index")
def index():
    my_in = Markup("<input type='text' name='uname'>")
    return render_template("index.html",
                           stu_info=STUDENT,
                           stu_list=STUDENT_LIST,
                           sd=STUDENT_DICT,m = my_in)




if __name__ == '__main__':
    app.run("0.0.0.0",9527)