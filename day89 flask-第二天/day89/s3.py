from flask import Flask, redirect, send_file
from serv.users import user
from serv.car import car

app = Flask(__name__)
app.config["DEBUG"] = True


@app.before_request # 请求进入视图函数之前
def be1():
    print("be1")
    # return "出错了"

    return None


@app.before_request # 请求进入视图函数之前
def be2():
    print("be2")
    return None


@app.after_request
def af1(res):
    print("af1")
    return res

@app.after_request
def af2(res):
    print("af2")
    return res

@app.errorhandler(404)
def error404(error_message):
    print(error_message)
    return send_file("linux.mp4")


app.register_blueprint(user)
app.register_blueprint(car)


if __name__ == '__main__':
    app.run()