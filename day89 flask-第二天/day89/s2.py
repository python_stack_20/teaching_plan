from flask import Flask, render_template, jsonify
from setting import DebugSetting
from setting import TesingSetting

app = Flask(__name__,template_folder="templates",static_folder="statics",static_url_path="/statics")

app.config.from_object(DebugSetting)
# app.config.from_object(TesingSetting)

#
# app.config["DEBUG"] = True
# app.debug = True


# app.config["JSONIFY_MIMETYPE"] = "123542146t/5g346hb32"
# app.config["TESTING"] = True

# app.config
# app.default_config


@app.route("/")
def index():
    # return render_template("index.html")
    return jsonify({"k":1})


if __name__ == '__main__':
    print(app.config)
    app.run()