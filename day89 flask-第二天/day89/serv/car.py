from flask import Blueprint

car = Blueprint("car",__name__,url_prefix="/car")


@car.route("/get_car",methods=["GET","post"])
def get_car():
    return "I am car Blueprint"

@car.route("/set_car",methods=["GET","post"])
def set_car():
    return "I am set car Blueprint"