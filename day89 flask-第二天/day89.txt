2019.7.11
为多个视图函数增加同一个装饰器

1.Flask中的路由
	endpoint 映射路由-视图函数__name__ = ""
	methods = ["get","Post"] 当前视图函数支持的请求方法 405 请求方式不被允许
	defaults = {"id":1} 默认参数 一旦默认参数存在 视图函数中必须有一个形参去接收 形参变量名必须与 defaults 中的一致
	strict_slashes=True 是否严格遵循路由匹配规则 "/"
	redirect_to = "/login" # 永久重定向 301 308 不经过视图函数的
	
	动态参数路由
	/detail/<folder>/<filename>

2.Flask中的配置
	1.初始化配置
		app = Flask(__name__)
		template_folder="templatess" # 更改模板存放目录 默认值是 templates
		static_folder="statics",	# 静态文件存放路径
		static_url_path="/static"   # 静态文件访问路径 - 默认是 "/"+static_folder
		区分 static_folder 和 static_url_path 之间的关系
		
		
	2.Config 对象配置
		app.default_config
		DEBUG --- 编码阶段 代码重启 日志输出级别很低 页面中会显示错误 错误代码
		TESTING --- 测试阶段	日志输出级别较高  无限接近线上环境
		class DebugSetting(object):
			DEBUG = True
			SECRET_KEY = "123456789"
			SESSION_COOKIE_NAME = "I am just Session"
			IAMXIAOHEI = "DSB"
			S20 = "NB"
			SESSION_TYPE = 'Redis'
			
		app.config.from_object(DebugSetting)
		
	
3.Flask 蓝图 Blueprint
	当成是一个不能够被run的Flask对象
	蓝图中是不存在Config
	
	蓝图需要注册在 app 实例上的
	
	app.register_blueprint(Blueprint实例)
	
	
	
4.Flask 特殊装饰器
	1.before_request 请求进入进入视图函数之前进行处理 return None 继续执行 否则阻断
	2.after_request 视图函数结束 响应客户端之前
	正常周期: be1 - be2 - be3 - vf - af3 - af2 - af1
	异常周期: be1 - af3 - af2 - af1

	3.errorhandler 重定义错误信息
	3.1.有参数的装饰器errorhandler(监听错误状态码 5xx 4xx Int)
	3.2.所装饰的函数必须有一个形参来接受 errorMessage
	

	
	




	
	