

# 昨日内容回顾

中间件

```python
from django.utils.deprecation import MiddlewareMixin
from django.urls import reverse
from django.shortcuts import HttpResponse,redirect
class MD1(MiddlewareMixin):

    def process_request(self,request):
        print('MD1')

        # return HttpResponse('你的请求有异常')#,None是正常的请求
    def process_view(self, request, view_func, view_args, view_kwargs):
        print('MD1的process_view')
        print(view_func.__name__) #index
        # return HttpResponse('ok')

    def process_response(self,request,response):
        # print(response)
        print('MD1的响应')
        # print(response.__dict__)

        return response  #接力棒
    def process_exception(self, request, exception):
        print('MD1的process_exception')
    def process_template_response(self, request, response):
        print("MD1 中的process_template_response")
        return response
        
settings配置文件
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'app01.custommiddleware.MD1',
    'app01.custommiddleware.MD2',
    # 'app01.custommiddleware.SessionAuth',

]
```

中间件的五个方法:

```
process_request(self,request)
process_view(self, request, view_func, view_args, view_kwargs)
process_template_response(self,request,response)
process_exception(self, request, exception)
process_response(self, request, response)
```



# 今日内容

## form组件

```
检验功能

前端生成HTML

还能保留用户输入的内容
```



```

{#{{ form_obj.as_p }}#} 将form_obj的所有字段都生成了标签
 {{ form_obj.title }} form_obj.字段名称  自动生成标签
```





# 今日内容总结

form

```python
from django import forms

简单使用:
# import re
# from django.core.exceptions import ValidationError
# def mobile_validate(value):
#    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
#    if not mobile_re.match(value):
#        raise ValidationError('手机号码格式错误')  
class MyForm(forms.Form):
	title = forms.CharField(  --- type=text   input框
	    label='xx',
	    initail ='默认值',
	    max_length = 32,
	    min_length=10,
	    error_message={
	    	'required':'不能为空',
	    	"max_length":'',
	    	"min_length":''
	    },
	    widget=forms.widgets.PasswordInput(attrs={'class':'xxx','type':'date'}),
	    help_text = 'xxxx',
	    # from django.core.validators import RegexValidator
	    # validators=[RegexValidator(r'^a','必须以a开头'),],
        # validators=[mobile_validate,],
	)
    

```

#### 选择框:

```python

sex = forms.ChoiceField(
        choices=(
            ('1','男'),
            ('2','女'),
        ),
        # widget=forms.widgets.RadioSelect()
        widget=forms.widgets.Select(attrs={'class': 'form-control'})

    )
author = forms.MultipleChoiceField(
        choices=(
            ('1', '男'),
            ('2', '女'),
        ),
        widget=forms.widgets.CheckboxSelectMultiple(),
    )
    
#将选择框数据指定成数据库中某个表的数据的操作
    #单选
    publish = forms.ModelChoiceField(
        queryset=models.Publish.objects.all(),
        # widget=forms.widgets.Select(attrs={'class': 'form-control'})
        widget=forms.widgets.Select()
    )
    #多选
    authors = forms.ModelMultipleChoiceField(
        queryset=models.Author.objects.all(),
        # widget = forms.widgets.SelectMultiple(attrs={'class':   'form-control'})
        widget = forms.widgets.SelectMultiple()
    ) 
```

form类的__init__方法的使用

```python
#指定choices值,forms.ChoiceField(#choices=(('1','xx'),))
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['publish'].choices = 			models.Publish.objects.all().values_list('pk','name') #[(1,'红浪漫'),]

#批量添加样式
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
```



```python
功能1:生成HTML标签
def home(request):
    if request.method == 'GET':
        form_obj = TestForm()  #通过form_obj来生成HTML标签
        return render(request,'home.html',{'form_obj':form_obj})
html文件中:
	{{ form_obj.title }} --- input标签
	{{ form_obj.title.label }}
	{{ form_obj.title.errors.0 }}  --- 单个字段的错误
	
def home(request):
	data = request.POST
    form_obj = TestForm(data)
    if form_obj.is_valid():  --- 这是验证方法 --- 
    	print(form_obj.cleaned_data) #{'name': '九阴真经'}
    #{'name2': 'asdf'}  model--name

    else:
    	return render(request,'home.html',{'form_obj':form_obj})


```



#### 局部钩子和全局钩子

```python

class Myform(forms.Form):
	title=xxx
	price=..
	局部钩子
	def clean_title(self):
	    val = self.cleaned_data.get('title')
		如果错了
            raise ValidationError('光喊6是不行的!!')
		没错:
			return val
	
	全局钩子
	def clean(self):
	    self.cleaned_data
	    
	    return = self.cleaned_data
	    
	    
```





































