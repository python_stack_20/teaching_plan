from django.shortcuts import render,HttpResponse,redirect
from django import forms
# Create your views here.
from app01 import models

class MyForm(forms.Form):
    title = forms.CharField(
        max_length=32,min_length=2,
        # required=False,
        # initial='张三',
        help_text='这里是输入用户名的地方,别忘了不能小于2位!',
        error_messages={
            "min_length":'长度不能小于2',
            "required":'该字段不能为空!',
            "max_length":'字段过长,不能超过32位!'
        },
        label='书名',
        widget=forms.widgets.TextInput(attrs={'placeholder':'这里是输入用户名的地方,别忘了不能小于2位!'})
        # widget=forms.widgets.PasswordInput()
    )
    price=forms.IntegerField(
        widget=forms.widgets.NumberInput()
    )
    publishDate = forms.DateField(
        # widget=forms.widgets.DateInput(attrs={'class': 'form-control','type':'date'})
        widget=forms.widgets.DateInput(attrs={'type':'date'})
    )

    # sex = forms.ChoiceField(
    #     choices=(
    #         ('1','男'),
    #         ('2','女'),
    #     ),
    #     # widget=forms.widgets.RadioSelect()
    #     widget=forms.widgets.Select(attrs={'class': 'form-control'})
    #
    # )
    # author = forms.MultipleChoiceField(
    #     choices=(
    #         ('1', '男'),
    #         ('2', '女'),
    #     ),
    #     widget=forms.widgets.CheckboxSelectMultiple(),
    # )

    #这种方式要通过__init__方法来指定choices
    # publish = forms.ChoiceField(
    #
    # )

    publish = forms.ModelChoiceField(
        queryset=models.Publish.objects.all(),
        # widget=forms.widgets.Select(attrs={'class': 'form-control'})
        widget=forms.widgets.Select()
    )

    authors = forms.ModelMultipleChoiceField(
        queryset=models.Author.objects.all(),
        # widget = forms.widgets.SelectMultiple(attrs={'class':   'form-control'})
        widget = forms.widgets.SelectMultiple()
    )
    # email = forms.EmailField(
    #     error_messages={
    #         'invalid':'请输入正确的邮箱格式'
    #     }
    # )

    # #指定choices值
    # def __init__(self,*args,**kwargs):
    #     super().__init__(*args,**kwargs)
    #     self.fields['publish'].choices = models.Publish.objects.all().values_list('pk','name') #[(1,'红浪漫'),]
    #
    # #批量添加样式
    # def __init__(self,*args,**kwargs):
    #     super().__init__(*args,**kwargs)
    #     for field in self.fields:
    #         self.fields[field].widget.attrs.update({
    #             'class': 'form-control'
    #         })

def index(request):

    if request.method == 'GET':
        form_obj = MyForm()

        return render(request, 'index.html', {'form_obj': form_obj})

    else:
        data= request.POST
        # print(data)
        form_obj = MyForm(data)
        if form_obj.is_valid(): #验证每个字段传过来的数据是不是正确的
            data = form_obj.cleaned_data
            print(data)
            author_data = data.pop('authors')
            print(author_data)
            book_obj = models.Book.objects.create(**data)
            book_obj.authors.add(*author_data)
            return HttpResponse('ok')

        else:
            print(form_obj.errors)
        return render(request, 'index.html', {'form_obj': form_obj})

import re
from django.core.exceptions import ValidationError

def mobile_validate(value):
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value):
        raise ValidationError('手机号码格式错误')  #自定义验证规则的时候，如果不符合你的规则，需要自己发起错误

from django.core.validators import RegexValidator

class TestForm(forms.Form):
    name = forms.CharField(
        max_length=32,
        # validators=[RegexValidator(r'^a','必须以a开头'),],
        # validators=[mobile_validate,],
        #{'name': '九阴真经'}
    )
    # price = forms.IntegerField()
    password = forms.CharField() #密码
    r_password = forms.CharField() #确认密码
    # name.errors
    #局部钩子
    def clean_name(self):
        value = self.cleaned_data.get('name') #{'name': '九阴真经'}
        if '666' in value:
            raise ValidationError('光喊6是不行的!!')
        else:
            return value
    # def clean_price(self):
    #     value = self.cleaned_data.get('price')

    #全局钩子
    def clean(self):
        p1 = self.cleaned_data.get('password')
        p2 = self.cleaned_data.get('r_password')
        if p1 == p2:
            return self.cleaned_data #return所有的cleaned_data
        else:
            self.add_error('r_password','两次输入的密码不一致')
            # raise ValidationError('两次输入的密码不一致!')

    # def clean_price(self):

def home(request):
    if request.method == 'GET':
        form_obj = TestForm()
        return render(request,'home.html',{'form_obj':form_obj})
    else:
        data = request.POST
        print(data)

        form_obj = TestForm(data)
        if form_obj.is_valid():
            print(form_obj.cleaned_data) #{'name': '九阴真经'}
            #{'name2': 'asdf'}  model--name


        else:
            return render(request,'home.html',{'form_obj':form_obj})












