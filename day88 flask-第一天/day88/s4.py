from datetime import timedelta

from flask import Flask,session,request,render_template,redirect
app = Flask(__name__)
app.secret_key = "#$%^&*#$%^&#$%2213123^&"
app.debug = True
app.session_cookie_name = "I am Not Session"
# app.testing = True
# app.permanent_session_lifetime = 15

STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'}

STUDENT_LIST = [
    {'name': 'Old', 'age': 38, 'gender': '中'},
    {'name': 'Boy', 'age': 73, 'gender': '男'},
    {'name': 'EDU', 'age': 84, 'gender': '女'}
]

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}


@app.route("/login",methods=["POST","GET"])
def login():
    if request.method == "GET":
        return render_template("login.html")

    uname = request.form.get("username")
    pwd = request.form.get("pwd")

    if uname == "123" and pwd == "123":
        session["username"] = "先帝"
        session["12e1"] = "创业"
        session["asdfas"] = "未半"
        session["username3"] = "挂了"
        session["username4"] = "此城"
        session["username5"] = "益州"
        session["username6"] = "疲弊"
        return "200 OK"
    else:
        return "失败"


@app.route("/detail")
def detail():
    # session["u"]
    if session.get("username"):
        return render_template("index.html",
                               stu_info=STUDENT,
                               stu_list=STUDENT_LIST,
                               sd=STUDENT_DICT)
    else:
        return redirect("/login")


if __name__ == '__main__':
    app.run("0.0.0.0",9527)