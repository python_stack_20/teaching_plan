import os

from flask import Flask, render_template,request

app = Flask(__name__)
# app.config["DEBUG"] = True
app.debug = True


@app.route("/login",methods=["POST","GET"])
def login():
    if request.method == "GET": # 请求方式

        print(request.url) # 获取访问路径
        print(request.method) # 获取请求方式
        print(request.path) # 路由地址 /login
        print(request.values) # 可以获取URL中的参数 也可以获取 FormData中的数据
        # 综合获取 X
        print(request.args.get("id")) # 获取URL中的参数
        print(request.args["id"]) # 获取URL中的参数
        print(request.args.to_dict()) # 获取URL中的参数 转换成 字典

        print(request.environ) # 获取请求原始信息
        print(request.base_url) # 获取URL头,不包含参数

        print(request.json) # 毁三观 1 请求头中 Content-type:application/json 数据序列化 request.json
        print(request.data) # 毁三观 2 请求头中 Content-type 不包含 Form or data

        print(request.headers) # 请求头中的数据




        return render_template("login.html")

    if request.method == "POST":
        print(request.headers)
        print(request.values.to_dict())  # 可以获取URL中的参数
        # print(request.form.to_dict())
        username = request.form.get("username")
        password = request.form["pwd"]
        print(request.form)
        # print(request.json) # 毁三观 1 请求头中 Content-type:application/json 数据序列化 request.json
        # print(request.data) # 毁三观 2 请求头中 Content-type 不包含 Form or data 原始请求体数据

        print(request.files.get("my_file"))
        my_file = request.files.get("my_file")
        # fp = os.path.join("templates",my_file.filename)
        my_file.save(my_file.filename)

        if username == "123" and password == "456":
            return "登录成功"
        else:
            return "登录失败"


if __name__ == '__main__':
    app.run()