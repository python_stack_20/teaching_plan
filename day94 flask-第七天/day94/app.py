from flask import Flask,request,jsonify
from db_conn import db

app = Flask(__name__)

@app.route("/login",methods=["post"])
def login():
    user_info = request.form.to_dict()

    res = db.users.find_one(user_info)
    if res:
        res["_id"] = str(res.get("_id"))
        return jsonify({"code": 0,"msg":"登录成功","data":res})
    else:
        user_info["nick"] = "小黑"
        db.users.insert_one(user_info)
        return {"code":1,"msg":"注册并登录成功"}



if __name__ == '__main__':
    app.run("0.0.0.0",9527)