import os

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "formtest.settings")
    import django

    django.setup()

    from app01 import models
    import random

    l1 = []
    for i in range(1, 101):
        obj = models.Book(
            title='随便%s'%i,
            price= 1 + i,
            publishDate='2018-06-0%s'%(random.randint(1,9)),
            publish_id=random.randint(1,3),
        )
        l1.append(obj)
    models.Book.objects.bulk_create(l1)