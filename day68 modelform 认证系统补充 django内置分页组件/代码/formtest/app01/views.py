from django.shortcuts import render,HttpResponse,redirect
from django import forms
# Create your views here.
from app01 import models

class MyForm(forms.Form):
    title = forms.CharField(
        max_length=32,min_length=2,
        help_text='这里是输入用户名的地方,别忘了不能小于2位!',
        error_messages={
            "min_length":'长度不能小于2',
            "required":'该字段不能为空!',
            "max_length":'字段过长,不能超过32位!'
        },
        label='书名',
        widget=forms.widgets.TextInput(attrs={'placeholder':'这里是输入用户名的地方,别忘了不能小于2位!'})
    )
    price=forms.IntegerField(
        widget=forms.widgets.NumberInput()
    )
    publishDate = forms.DateField(
        widget=forms.widgets.DateInput(attrs={'type':'date'})
    )

    publish = forms.ModelChoiceField(
        queryset=models.Publish.objects.all(),
        widget=forms.widgets.Select()
    )

    authors = forms.ModelMultipleChoiceField(
        queryset=models.Author.objects.all(),
        widget = forms.widgets.SelectMultiple()
    )
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })

class MyModelForm(forms.ModelForm):

    class Meta:
        model = models.Book
        # fields = ['title','']
        fields = '__all__'
        # exclude = ['title',]
        labels = {
            'title':'书名',
            'price':'价格',
        }
        error_messages = {
            'title':{
                'required':'不能为空',
                'max_length':'太长了',
            }
        }
        widgets = {  #字段定制插件
            'publishDate':forms.widgets.DateInput(attrs={'type':'date'}),
            # 'publishDate2':forms.widgets.DateInput(attrs={'type':'date'}),
        }


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


def create_book(request):
    if request.method == 'GET':
        form_obj = MyForm()
        # form_obj = MyModelForm()
        return render(request, 'index.html', {'form_obj': form_obj})
    else:
        data= request.POST
        form_obj = MyForm(data)
        # form_obj = MyModelForm(data)
        if form_obj.is_valid(): #验证每个字段传过来的数据是不是正确的
            data = form_obj.cleaned_data
            author_data = data.pop('authors')
            book_obj = models.Book.objects.create(**data)
            book_obj.authors.add(*author_data)
            return redirect('show')
        else:
            print(form_obj.errors)
        return render(request, 'index.html', {'form_obj': form_obj})

from django.core.paginator import Paginator

show_page_range = 5  #页面显示的页码数量
every_page_count = 5 #每页显示多少条数据

def show(request):

    all_books = models.Book.objects.all()

    page_obj = Paginator(all_books,every_page_count) #5每页显示多少条

    print(page_obj.num_pages) #21
    print(page_obj.page_range) #页码从多少到多少,range(1,22)
    print(page_obj.count) #总共有多少条数据 104
    current_page_num = request.GET.get('page',1)
    # page1 = page_obj.page(1)  #第一页所有的model对象数据
    # for i in page1:
    #     print(i)
    # page2 = page_obj.page(2)
    # print(page2) #<Page 2 of 21>
    # print(page2.has_previous()) #True
    # print(page2.has_next()) #True

    page_range = page_obj.page_range
    page_num_objs = page_obj.page(current_page_num)
    all_numbers = page_obj.num_pages

    # if page_num_objs.has_next():
    # print('下一页页码',page_num_objs.next_page_number())
    # print('上一页页码',page_num_objs.previous_page_number())

    if all_numbers > show_page_range : #如果总页码大于5页,咱们就让他显示5个页码
        current_page_num = int(current_page_num)
        if current_page_num <= 3:
            page_range = range(1,6)
        elif current_page_num + 2 > all_numbers:
            page_range = range(all_numbers-4, all_numbers+1)
        else:
            page_range = range(current_page_num - 2, current_page_num + 3)


    # all_books = models.Book.objects.all().first()
    # all_books.authors.all() #多对多
    # all_books.publish.name #一对多
    return render(request,'show.html',{'page_num_objs':page_num_objs,'page_range':page_range,'current_page_num':current_page_num})

def edit_book(request,n):
    book_obj = models.Book.objects.filter(pk=n)

    if request.method == 'GET':
        book_obj = book_obj.first()
        form_obj = MyModelForm(instance=book_obj)
        return render(request,'edit_book.html',{'n':n,'form_obj':form_obj})

    else:
        data = request.POST
        form_obj = MyModelForm(data,instance=book_obj.first())
        if form_obj.is_valid():
            form_obj.save()  #update--book_obj.update(**data)
            return redirect('show')
        else:
            return render(request,'edit_book.html',{'form_obj':form_obj,'n':n})

def delete_book(request,n):
    models.Book.objects.filter(pk=n).delete()
    return redirect('show')
from django.http import JsonResponse

def ajax_delete_book(request,n):
    data = {'status':0}
    try:
        data['status'] = 1
        models.Book.objects.filter(pk=n).delete()
    except Exception:
        data['status'] = 2
    return JsonResponse(data)







def test(request):

    new_obj = models.Student.objects.get(name='chao2')
    print(new_obj.sex) # 1
    print(new_obj.get_sex_display()) # 1
    print(new_obj.name) #chao

    return render(request,'testchoice.html',{'new_obj':new_obj})
