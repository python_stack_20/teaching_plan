# 昨日内容回顾

## 认证系统

auth模块,auth_user表

```
python manage.py createsuperuser 
用户名:
邮箱,可以用不写
密码:必须超过8位

from django.contrib.auth.models import User
User.object.create_user(username='xx',password='xxxx') #在user表里加一条 记录,并且密码是加密的

#找到了就返回这个用户对象,没找到返回None
user_obj = auth.authenticate(username=username,password=password)

 #维持会话用的,session,request.user = user_obj
auth.login(request,user_obj)

#注销  
auth.logout(request)

 #判断用户是否已经登录了
request.user.is_authenticated():  登录了返回True,没登录返回Fasle

#检查当前用户提交的旧密码是否正确
request.user.check_password(old_pwd)

#修改密码
request.user.set_password(new_pwd) #修改密码
request.user.save() #保存

#判断 用户名是否存在
User.objects.filter(username=username,).exists()

User表的拓展
1.继承  2.onotoone(to='User')
from django.contrib.auth.models import AbstractUser
class UserInfo(AbstractUser):
	id_card = models.CharField(max_length=32)
	...

settings.py
AUTH_USER_MODEL = "app01.UserInfo"  #应用名称.表名

然后执行数据库同步指令


```



# 今日内容

补充一个models表字段中的choices

```python
class Student(models.Model):

    name = models.CharField(max_length=12,null=True)
    sex_choice = ((1,'男'),(2,'女'),)

    sex = models.IntegerField(
        choices=sex_choice,
    )

python代码
def test(request):

    new_obj = models.Student.objects.get(name='chao2')
    print(new_obj.sex) # 1
    print(new_obj.get_sex_display()) # 1
    print(new_obj.name) #chao

    return render(request,'testchoice.html',{'new_obj':new_obj})


模板语法里面的写法
{{ new_obj.get_sex_display }}

```



## modeform

```python
#创建modelform类
from django import forms
class MyModelForm(forms.ModelForm):
    class Meta:
        model = models.Book  #一个表对应一个modelform
        # fields = ['title','']  #选择你需要验证的字段
        fields = '__all__'      #选择所有字段
        # exclude = ['title',]  #排除某些字段
        labels = {   #注意写法
            'title':'书名',
            'price':'价格',
        }
        error_messages = {  #定制错误信息
            'title':{
                'required':'不能为空',
                'max_length':'太长了',
            },
            ...
            
        }
        widgets = {  #字段定制插件
            'publishDate':forms.widgets.DateInput(attrs={'type':'date'}),
            # 'publishDate2':forms.widgets.DateInput(attrs={'type':'date'}),
        }
	
    def clean_title(self): #局部钩子
		val = self.cleaned_data.get('titile')
        return val
    def clean(self):pass 全局钩子
    
	#照样可以重写init方法来进行批量操作
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
 
            
            
```



使用modelform类

```python
def edit_book(request,n):
    book_obj = models.Book.objects.filter(pk=n)
    if request.method == 'GET':
        book_obj = book_obj.first()
        form = MyModelForm(instance=book_obj) #实例化modelform对象,其中参数是你要求改的那个model对象,如果不加instance=book_obj就只生成标签,没有数据
        
        return render(request,'edit_book.html',{'n':n,'form':form})

    else:
        data = request.POST
        form = MyModelForm(data,instance=book_obj.first()) #如果没有写instance=这个参数,那么save方法会翻译成create,添加
        if form.is_valid():
            form.save()  #update--book_obj.update(**data)
            return redirect('show')
        else:
            return render(request,'edit_book.html',{'form':form,'n':n})
            
```



HMTL文件的写法

```html
form
{% for field in form %}
	<div class="form-group {% if field.errors.0 %} has-error {% endif %}">
		<label for="{{ field.id_for_label }}"> {{ field.label }}</label>

		{{ field }}
		<span class="text-success">{{ field.help_text }}</span>
		<span class="text-danger">{{ field.errors.0 }}</span>
	</div>
{% endfor %}

```



需求:

```
基于form组件的注册
基于ajax的登录

用户名: 最大不能超过32位,最少不能少于6位,不能为空
密码: 最大不能超过32位,最少不能少于6位,不能为空
确认密码: 最大不能超过32位,最少不能少于6位,不能为空
邮箱:最大不能超过32位,邮箱格式要争取

以拓展auth_user表的方式来创建用户信息表
认证,使用django的认证系统

```

验证码:

```python
def get_valid_img(request):

    from PIL import Image
    #终极版，方式5
    import random
    def get_random_color():
        return (random.randint(0,255),random.randint(0,255),random.randint(0,255))
    from PIL import Image,ImageDraw,ImageFont
    img_obj = Image.new('RGB', (200, 34), get_random_color()) #图片对象
    draw_obj = ImageDraw.Draw(img_obj)  #通过图片对象生成一个画笔对象
    font_path = os.path.join(settings.BASE_DIR,'statics/font/NAUERT__.TTF') #获取字体,注意有些字体文件不能识别数字，所以如果数字显示不出来，就换个字体
    font_obj = ImageFont.truetype(font_path,16) #创建字体对象
    sum_str = ''  #这个数据就是用户需要输入的验证码的内容
    for i in range(6):
        a = random.choice([str(random.randint(0,9)), chr(random.randint(97,122)), chr(random.randint(65,90))])  #4  a  5  D  6  S
        sum_str += a
    print(sum_str)
    draw_obj.text((64,10),sum_str,fill=get_random_color(),font=font_obj) #通过画笔对象，添加文字

    width=200
    height=34
    # 添加噪线
    for i in range(5): #添加了5条线
        #一个坐标表示一个点，两个点就可以连成一条线
        x1=random.randint(0,width)
        x2=random.randint(0,width)
        y1=random.randint(0,height)
        y2=random.randint(0,height)
        draw_obj.line((x1,y1,x2,y2),fill=get_random_color())
    # # 添加噪点
    for i in range(10):
        #这是添加点，50个点
        draw_obj.point([random.randint(0, width), random.randint(0, height)], fill=get_random_color())
        #下面是添加很小的弧线，看上去类似于一个点，50个小弧线
        x = random.randint(0, width)
        y = random.randint(0, height)
        draw_obj.arc((x, y, x + 4, y + 4), 0, 90, fill=get_random_color()) #x, y是弧线的起始点位置，x + 4, y + 4是弧线的结束点位置

    from io import BytesIO
    f = BytesIO()  #操作内存的把手
    img_obj.save(f,'png')  #
    data = f.getvalue()

    # 存这个验证码的方式1：赋值给全局变量的简单测试
    # global valid_str
    # valid_str = sum_str
    # 方式2：将验证码存在各用户自己的session中,session的应用其实还有很多
    request.session['valid_str'] = sum_str
    return HttpResponse(data)
```



认证系统中的认证装饰器

引入:

```
from django.contrib.auth.decorators import login_required
```

使用:

```
@login_required  #自动会跳转一个路径
def home(request):
    # if request.user.is_authenticated(): #判断用户是否已经登录了
    return render(request,'home.html')
```

配置跳转路径:

settings配置文件:

```
LOGIN_URL = '/login/'
```

## Django内置的分页组件























