# 昨日内容回顾

路由系统url

```
    无名分组
    url(r'^articles/(\d{4})/(\d{1,2})/$', views.articles_year_month)
    def articles_year_month(request,n1,n2):
    	pass
    有名分组
	url(r'^articles/(?P<year>\d{4})/(?P<month>\d{1,2})/$', views.articles_year_month),
    def articles_year_month(request,year,month):
    	pass	
	
	默认参数
	    def articles_year_month(request,month,year='2012'):
    	pass
	额外参数
	url(r'^articles/(?P<year>\d{4})/(?P<month>\d{1,2})/$', views.articles_year_month,{'foo':'xxx'})
	
	def articles_year_month(request,month,foo,year='2012'):pass
	
	
```



路由分发include

```
项目的路由urls.py里面
app01,app02
1 导入include
2 app文件夹中创建每个app的urls.py文件
3 url(r'^app01/', include('app01.urls'))

url(r'^$', views.home), #匹配首页
```





今日内容

视图

CBV 和 FBV

```
FBV:def index(request):pass

CBV:
from django.views import View
class LoginView(View): #继承View
	
	#重写dispatch方法的写法
	def dispatch(self,request,*args,**kwargs)
		#执行父类的dispatch方法
		ret = super().dispatch(request,*args,**kwargs)
		return ret  #别忘了return值
		
    def get(self,request): #根据用户的请求方法,找到对应的方法

        return render(request,'login.html')

    def post(self,request):
        print(request.POST)
        uname = request.POST.get('username')
        pwd = request.POST.get('password')
        if uname == 'chao' and pwd == '123':
            return render(request, 'home.html')
        return HttpResponse('用户名或者密码错误')
        
urls.py
 	url(r'^login/', views.LoginView.as_view()), 1.as_view()--view,2用户请求到了 view,3view里面执行了dispatch,反射找到请求方法对应的自己视图的方法
 	
 	
FBV装饰器
#装饰器
def xxx(func1):
    def inner(request,*args,**kwargs):
        print('xx1')
        ret = func1(request,*args,**kwargs)
        print('xx2')
        return ret
    return inner
@xxx
def index(request):pass

CBV
from django.utils.decorators import method_decorator

方式三:
@method_decorator(xxx,name='get')
class IndexView(View):
	方式一:
	@method_decorator(xxx)
	def dispatch(...):....
	
	方式二:
	@method_decorator(xxx)
	def get(self,request):pass
	def post(self,request):pass

```

request  请求对象

```
request.body:得到用户提交数据的原始数据类型 b'username=chao&passowrd=123'
request.GET
request.POST
request.method 获取请求方法
request.path_info  获取用户请求路径
request.get_full_path() 获取除域名之外的全路径,包括请求参数
request.META  获取请求头的元信息

```

response 响应对象

```
三个方法
HttpResponse('xxx')  返回一个字符串
render(request,'index.html',{'name':'chao'})  index.html  {{ name }}--chao
redirect('/login/') -- 给浏览器发送了一个重定向的请求,浏览器拿到你要重定向的url,然后自动发送了一个ip+端口+路径/login/,的一个请求,后端在配置对应的url('^login/',views.login)

```



模板语言

```
{{ 变量 }} {% 逻辑 %}
```

过滤器

```
{{ 变量|过滤器名称:'过滤器需要的参数' }}
safe
date


```

















