from django.shortcuts import render,HttpResponse,redirect
from django.views import View
# Create your views here.

#装饰器
def xxx(func1):
    def inner(request,*args,**kwargs):
        print('xx1')
        ret = func1(request,*args,**kwargs)
        print('xx2')
        return ret
    return inner


#FBV模式的视图的写法
# @xxx
# def login(request):
#
#     if request.method =='GET':
#         return render(request,'login.html')
#
#     else:
#         print(request.POST)
#         uname = request.POST.get('username')
#         pwd = request.POST.get('password')
#         if uname =='chao' and pwd == '123':
#             return render(request,'home.html')
#
#         return HttpResponse('用户名或者密码错误')



from django.utils.decorators import method_decorator

# CBV模式视图的写法
# @method_decorator(xxx,name='get')
class LoginView(View):
    # name='chao'
    @method_decorator(xxx) #方式一
    def dispatch(self, request, *args, **kwargs):
        # print('前')
        ret = super().dispatch(request, *args, **kwargs)
        # print('后')
        return ret

    def get(self,request):
        # self.xx(request)
        # return render(request,'login.html',{'name':self.name})
        return render(request,'login.html')

    @method_decorator(xxx) #方式二
    def post(self,request):
        print(request.POST)
        uname = request.POST.get('username')
        pwd = request.POST.get('password')
        if uname == 'chao' and pwd == '123':
            return render(request, 'home.html')

        return HttpResponse('用户名或者密码错误')

    # def xx(self,request):
    #     print('xxx')


# def login2(request):
#     # uname = request.POST.get('username')
#     # pwd = request.POST.get('password')
#     # if uname == 'chao' and pwd == '123':
#     #     return render(request, 'home.html')
#     #
#     # return HttpResponse('用户名或者密码错误')
#
#     print(request.GET)
#     uname = request.GET.get('username')
#     pwd = request.GET.get('password')
#     if uname == 'chao' and pwd == '123':
#         return render(request, 'home.html')
#
#     return HttpResponse('用户名或者密码错误')



def index(request):
    print(request.method)
    print(request.POST)
    print(request.GET)
    print(request.body) #b'username=asdf'
    print(request.path_info) #/index/  路径
    print(request.get_full_path()) #/index/?username=chao 路径
    print(request.META.get('HTTP_USER_AGENT')) #META请求头元信息
    print(request.META.get('REMOTE_ADDR'))  #客户端IP地址



    return HttpResponse('ok')

    # return redirect('/login/') #重定向

    # return render(request,'index.html')










