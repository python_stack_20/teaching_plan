
from wsgiref.simple_server import make_server
from urls import urlpatterns


def application(environ, start_response):

    start_response('200 OK', [('Content-Type', 'text/html')])
    path = environ['PATH_INFO']
    for urlpattern in urlpatterns:
        if path == urlpattern[0]:
            data = urlpattern[1](environ) #login(environ)
            break
    else:
        data = b'sorry 404!,not found the page'
    return [data]

#和咱们学的socketserver那个模块很像啊
httpd = make_server('127.0.0.1', 8001, application)

print('Serving HTTP on port 8001...')
# 开始监听HTTP请求:
httpd.serve_forever()