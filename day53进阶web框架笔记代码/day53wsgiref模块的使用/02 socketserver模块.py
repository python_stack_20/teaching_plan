
import socketserver                              #1、引入模块
class MyServer(socketserver.BaseRequestHandler): #2、自己写一个类，类名自己随便定义，然后继承socketserver这个模块里面的BaseRequestHandler这个类

    def handle(self):                            #3、写一个handle方法，必须叫这个名字
        #self.request                            #6、self.request 相当于一个conn

        self.request.recv(1024)                  #7、收消息
        msg = '亲，学会了吗'
        self.request.send(bytes(msg,encoding='utf-8')) #8、发消息

        self.request.close()                     #9、关闭连接

        # 拿到了我们对每个客户端的管道，那么我们自己在这个方法里面的就写我们接收消息发送消息的逻辑就可以了
        pass
if __name__ == '__mian__':
    #thread 线程，现在只需要简单理解线程，别着急，后面很快就会讲到啦，看下面的图
    server = socketserver.ThreadingTCPServer(('127.0.0.1',8090),MyServer)#4、使用socketserver的ThreadingTCPServer这个类，将IP和端口的元祖传进去，还需要将上面咱们自己定义的类传进去，得到一个对象，相当于我们通过它进行了bind、listen
    server.serve_forever()                       #5、使用我们上面这个类的对象来执行serve_forever()方法，他的作用就是说，我的服务一直开启着，就像京东一样，不能关闭网站，对吧，并且serve_forever()帮我们进行了accept