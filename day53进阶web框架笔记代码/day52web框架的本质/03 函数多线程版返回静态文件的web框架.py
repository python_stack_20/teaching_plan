import socket
from threading import Thread
server = socket.socket()
server.bind(('127.0.0.1',8001))
server.listen()


def html(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    with open('01 index2.html', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()
def css(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    with open('index.css', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()

def js(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    with open('index.js', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()

def ico(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    with open('favicon.ico', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()
def jpg(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    # conn.send(b'who are you?')
    with open('1.jpg', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()


urlpatterns = [
    ('/',html),
    ('/index.css',css),
    ('/index.js',js),
    ('/favicon.ico',ico),
    ('/1.jpg',jpg),
]


def communication(conn):
    client_msg = conn.recv(1024).decode('utf-8')
    # print(client_msg)
    path = client_msg.split('\r\n')[0].split(' ')[1]
    # print(path)
    #针对不同的请求路径,返回不同的文件
    for urlpattern in urlpatterns:
        if urlpattern[0] == path:
            # urlpattern[1](conn)
            # 多线程执行函数
            t = Thread(target=urlpattern[1],args=(conn,))
            t.start()

    # if path =='/':
    #     html(conn)
    # elif path =='/index.css':
    #     css(conn)
    # elif path =='/index.js':
    #     js(conn)
    # elif path =='/favicon.ico':
    #     ico(conn)
    # elif path =='/1.jpg':
    #     jpg(conn)


while 1:
    conn, add = server.accept()
    t = Thread(target=communication,args=(conn,))
    t.start()


























