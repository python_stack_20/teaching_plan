import socket
from threading import Thread
server = socket.socket()
server.bind(('0.0.0.0',8001))
server.listen()
def communication(conn):
    client_msg = conn.recv(1024).decode('utf-8')
    # print(client_msg)
    path = client_msg.split('\r\n')[0].split(' ')[1]
    print(path)
    #针对不同的请求路径,返回不同的文件
    if path =='/':
        conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
        # conn.send(b'who are you?')
        with open('01 index2.html', 'rb') as f:
            data = f.read()
        conn.send(data)
        conn.close()
    elif path =='/index.css':
        conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
        # conn.send(b'who are you?')
        with open('index.css', 'rb') as f:
            data = f.read()
        conn.send(data)
        conn.close()
    elif path =='/index.js':
        conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
        # conn.send(b'who are you?')
        with open('index.js', 'rb') as f:
            data = f.read()
        conn.send(data)
        conn.close()
    elif path =='/favicon.ico':
        conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
        # conn.send(b'who are you?')
        with open('favicon.ico', 'rb') as f:
            data = f.read()
        conn.send(data)
        conn.close()

    elif path =='/1.jpg':
        conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
        # conn.send(b'who are you?')
        with open('1.jpg', 'rb') as f:
            data = f.read()
        conn.send(data)
        conn.close()


while 1:
    conn, add = server.accept()
    t = Thread(target=communication,args=(conn,))
    t.start()























