# 昨日内容回顾

今日内容回顾

```
事务  
全局  settings配置

局部
	from django.db import transaction
	视图函数装饰器的形式
		@transaction.atomic
		def view()...
    上下文的:
    	with transaction.atomic():
    		sql.....
    		
select_for_update()
select * from t1  for update
models.T1.objects.select_for_update().filter(id=1)  #排他锁(互斥锁)  读写不行写也不行
```



ajax

```js
异步发请求
局部刷新
jquery版的:
$.ajax({
	var username = $('#username').val();
	var csrf_data = $('[name="csrfmiddlewaretoken"]').val()
	url:'{% url 'home' %}',
	type:'post',
	data:{
		uname:username,
		csrfmiddlewaretoken:csrf_data,
	},
	success:function(ret){
		
	
	}

})



```



文件上传

form表单上传文件

```
enctype="multipart/form-data  告诉http,请求后端服务端的时候,发送的请求体里面的数据格式是multipart/form-data,这个格式发送的是片段数据,content-type

http:
contenttype : application/x-www-form-urlencoded  默认的 
a=1&b=2&c=3  get url+数据
			 post  请求体  

multipart/form-data  

application/json


```



## 今日内容回顾



 form文件上   form  enctype='mutiple-form....'    

views:

​	request.FILES.get('标签name值')



ajax文件上传

​    var formdata  = new FormData();

​    formdata .append('file_obj',$('input[type=file]')[0].files[0]);

​	 formdata .append('csrfmiddlewaretoken',$('[name=csrfmiddlewaretoken]').val());

​	 formdata .append('username',$('[name=username]').val());

```
$.ajax({

    url:'路径',
	type:'post',
	data:formdata,
	processData: false ,    // 不处理数据
    contentType: false,    // 不设置内容类型
	success:function(res){}

})

file_obj = request.FILES.get('file_obj')

with open(file_obj.name,'wb') as f:
	for data in file_obj:
		f.write(data)
	
	for cc in file_obj.chunks():
		f.write(cc)


```



contenttype

三种类型



响应方法

```
HttpResponse(json.dumps({'name';'chao'}),content_type='json')
JsonResponse({'name';'chao'})
其他数据类型
JsonResponse({'name';'chao'},safe=False)

序列化
    #建议这种方式来序列化我们的models数据
    # book_objs = models.Book.objects.all().values('title','price')
    # books = list(book_objs)


时间日期序列化
class JsonCustomEncoder(json.JSONEncoder):
    def default(self, field):
        if isinstance(field,datetime):
            return field.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(field,date):
            return field.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self,field)

a_json = json.dumps(a,cls=JsonCustomEncoder)


```



跳转页面

```
    //跳转页面
            var url = res.url;
            location.href = url;
```





