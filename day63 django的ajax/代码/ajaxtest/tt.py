# a = b'{"username":"chao","password":"123"}'
# a = a.decode('utf-8')
# import json
# a_json = json.loads(a)
# print(a,type(a))
# print(a_json,type(a_json))
import json
from datetime import datetime,date
a = {'name':'chao','timer':datetime.now()}


class JsonCustomEncoder(json.JSONEncoder):
    def default(self, field):
        if isinstance(field,datetime):
            return field.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(field,date):
            return field.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self,field)

a_json = json.dumps(a,cls=JsonCustomEncoder)
print(a_json)








