from django.shortcuts import render,HttpResponse,redirect
from ajaxtest import settings
# Create your views here.
from django.http import JsonResponse
from django.core import serializers
from django.urls import reverse
from app01 import models
def login(request):

    return render(request,'login.html')


def index(request):
    username = request.POST.get('uname')
    # username = request.GET.get('uname')
    print(username) #123
    status = '2'
    return HttpResponse(status)


# def upload(request):
#     if request.method == "POST":
#         print(request.POST)
#         file_obj = request.FILES.get('elephant')
#         print(file_obj,type(file_obj))
#
#         file_name = file_obj.name #1.jpg
#         # 拼接路径
#         import os
#         file_path = os.path.join(settings.BASE_DIR, 'statics' , 'img' , file_obj.name)
#
#         with open(file_path,'wb') as f:
#             # for data in file_obj:
#             #     # file_obj.read(1024)
#             #     f.write(data)
#             for chunk in file_obj.chunks():
#                 f.write(chunk)
#
#         return HttpResponse('ok')
#     else:
#         return render(request,'upload.html')


def upload(request):
    if request.method == "POST":
        print(request.POST)
        file_obj = request.FILES.get('file_obj')
        print(file_obj,type(file_obj))

        # if content-type=='multipart/form-data ':
        #     request.FILES

        file_name = file_obj.name #1.jpg
        # 拼接路径
        import os
        file_path = os.path.join(settings.BASE_DIR, 'statics' , 'img' , file_obj.name)

        with open(file_path,'wb') as f:
            # for data in file_obj:
            #     # file_obj.read(1024)
            #     f.write(data)
            for chunk in file_obj.chunks():
                f.write(chunk)

        return HttpResponse('ok')
    else:
        return render(request,'upload.html')


def home(request):

    return render(request,'home.html')


def home2(request):
    # xml
    # print(request.body) #b'{"username":"chao","password":"123"}'

    print(request.GET) #<QueryDict: {}>
    print(request.POST) #<QueryDict: {}>
    # username = request.GET.get('username')
    # password = request.GET.get('password')
    # print(username)
    # if request.META.get('content-type')  == 'urlencoded':
    #     request.body.split('&')  [a=1,b=2]
    #     request.POST[]
    #
    # cont chao
    #  a*1|b*2
    # print(password)

    # res = {'status':0,'info':[11,22,33]}
    # res = [11,22,33]
    #建议这种方式来序列化我们的models数据
    # book_objs = models.Book.objects.all().values('title','price')
    # books = list(book_objs)



    # print(books) #[{'title': 'jinpingmei', 'price': 200}, {'title': 'xiaoheishi', 'price': 1}]


    # res = serializers.serialize('json',book_objs)
    # print(res)
    #[{"model": "app01.book", "pk": 1, "fields": {"title": "jinpingmei", "price": 200}}, {"model": "app01.book", "pk": 2, "fields": {"title": "xiaoheishi", "price": 1}}]
    # res = {'status':0,'info':'xxx'}
    # import json
    # res_str = json.dumps(res)
    res = {'status':0,'data':{'name':'chao'},'url':reverse('upload')}

    # return HttpResponse(res_str,content_type='application/json')
    # return JsonResponse(res,safe=False)
    # return JsonResponse(books,safe=False)
    return JsonResponse(res)



