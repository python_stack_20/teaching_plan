from django.conf.urls import url
from web.views import customer
from web.views import payment
from web.views import account

urlpatterns = [

    #客户信息管理
    url(r'^customer/list/$', customer.customer_list,name='customer'),
    url(r'^customer/add/$', customer.customer_add,name='customeradd'),
    url(r'^customer/edit/(?P<cid>\d+)/$', customer.customer_edit,name='customeredit'),
    url(r'^customer/del/(?P<cid>\d+)/$', customer.customer_del,name='customerdel'),

    #缴费管理
    url(r'^payment/list/$', payment.payment_list,name='payment'),
    url(r'^payment/add/$', payment.payment_add,name='paymentadd'),
    url(r'^payment/edit/(?P<pid>\d+)/$', payment.payment_edit,name='paymentedit'),
    url(r'^payment/del/(?P<pid>\d+)/$', payment.payment_del,name='paymentdel'),

    url(r'^login/$',account.login,name='login')
]
