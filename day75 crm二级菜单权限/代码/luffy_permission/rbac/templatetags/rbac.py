from django import template
from django.urls import reverse
register = template.Library()

from django.conf import settings
import re

@register.inclusion_tag('rbac/menu.html')
def menu(request):
    # menu_list = request.session.get('permisson_menu_list')
    menu_dict= request.session.get('permisson_menu_dict')
    for key,item in menu_dict.items():
        item['class'] = 'hide'

        for child in item['children']:
            # /customer/add/
            # 'url': '/customer/list/'
            # 'url': '/payment/list/'
            # # if re.match('^{}$'.format(child['url']), request.path):
            if request.show_id == child['pk']:
                # null == child['pk'] 1

                item['class'] = ''
                child['class'] = 'active'
                break


    return {"menu_dict": menu_dict}



@register.filter
def haspermission(base_url,request):


    for item in request.session['permisson_list']:
        #item = {'url','','title':'','name':''}
        reg = '^%s$' % item['url']  #/customer/edit/(\d+)/ --- "/customer/edit/1/"
        ret = re.search(reg, base_url)
        # if item['name'] == base_url:
        if ret:
            return True

    return False





