# 昨日内容回顾

#### jQuery的引入

script  src='本地文件'  CDN网络地址		

#### DOM和jQuery对象之间的转换

dom---jquery   $(dom对象)

jquery --- dom  jquery对象[0]

#### 选择器

基础选择器

$('#d1')    $('.d1')  $('标签名')  $('*')    $('div,p') $('div.c1)

层级选择器

$('div p') ...

基本筛选器

```javascript
:first   $('div:first')   
:last    
:eq(索引) .eq(索引)
:even     索引为偶数的
:odd      索引为奇数的
:not(选择器)  排除满足not的
:has(选择器)  有has选择器能够找到的后代元素的父级标签
:gt(索引) 大于
:lt(索引) 小于
:not(:has(选择器))  排除有has选择器能够找到的后代元素的父级标签

```

#### 属性选择器

```javascript
$('[属性名]')

$('[属性名=属性值]')

$('[属性名!=属性值]')

$('input[属性名=属性值]')
```

表单筛选器

```
:text  --- input type='text'
...
```

表单对象属性

```
:enabled    可用的
:disabled   不可用的
:checked    被选中的input的
:selected   被选中的select标签里面的option标签
```

筛选器方法

```
找下一个
$('#d1').next()  
.nextAll()    找下面所有的
.nextUntil('选择器')  区间,一直找到满足这个选择器的标签,并且不包含它

上一个
.prev()
.prevAll()
.prevUntil(选择器)

父级的
.parent()
.parents()
.parentsUntil('选择器')

儿子
.children('可放选择器')

兄弟
.siblings('可放选择器')

find
$('.c1').find('p') 找到c1后代的p标签  类似于后代选择器 .c1 p

filter
$('.c1').filter('.c2')  从满足有c1类的标签中筛选出有c2类的标签 类似于组合选择器  .c1.c2

first()
....


```

绑定点击事件

对象.click(function(){})



#### 样式操作

样式类

```javascript
添加:对象.addClass('c1')  

删除:对象.removeClass('c1')

判断:对象.hasClass('c1')

切换:对象.toggleClass('c1')
```

直接样式操作css

```javascript
对象.css(属性名,属性值)
对象.css({'属性名':'属性值',...})
```



# position



值设置

```javascript
$('input[name=sex]').val(['1'])
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
$('input[name=sex]').val(['2'])
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
$(':radio').val(['1'])
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
$(':radio').val(['2'])
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
$(':checkbox').val(['1','2'])
jQuery.fn.init(4) [input, input, input, input, prevObject: jQuery.fn.init(1)]
$(':checkbox').val()
"1"
$(':checked')
jQuery.fn.init(4) [input, input, input, option, prevObject: jQuery.fn.init(1)]0: input1: input2: input3: optionlength: 4prevObject: jQuery.fn.init [document]__proto__: Object(0)
$(':checkbox:checked')
jQuery.fn.init(2) [input, input, prevObject: jQuery.fn.init(1)]
```



属性操作

```javascript
$('#d1').attr('age','18')
jQuery.fn.init [div#d1]
$('#d1').attr('age')
"18"
$('#d1').attr({'k1':'v1','k2':'v2'})
jQuery.fn.init [div#d1]
$('#d1').removeAttr('age')
jQuery.fn.init [div#d1]
```

```javascript
$(':checkbox')
jQuery.fn.init(3) [input, input, input, prevObject: jQuery.fn.init(1)]
$(':checkbox').eq(0).attr('checked','checked');
jQuery.fn.init [input, prevObject: jQuery.fn.init(3)]
$(':checkbox').eq(0).attr('checked');
"checked"
$(':checkbox').eq(1).attr('checked');
undefined
$(':checkbox').eq(1).prop('checked');
false
$(':checkbox').eq(0).prop('checked');
true
$(':checkbox').eq(1).prop('checked',true);
jQuery.fn.init [input, prevObject: jQuery.fn.init(3)]
$(':checkbox').eq(1).prop('checked',false);
jQuery.fn.init [input, prevObject: jQuery.fn.init(3)]
```

文档操作

```javascript
内部元素的后面
第一种写法
var a = document.createElement('a')
undefined
a
<a>​</a>​
$(a).attr('href','http://www.baidu.com')
jQuery.fn.init [a]
a
<a href=​"http:​/​/​www.baidu.com">​</a>​
$(a).text('百度')
jQuery.fn.init [a]
a
<a href=​"http:​/​/​www.baidu.com">​百度​</a>​
$('#d1').append(a)
jQuery.fn.init [div#d1]
第二种写法
$(a).appendTo('#d1')

扩展写法:重点
$('#d1').app
undefined
$('#d1').append('<a href="http://www.baidu.com">百度</a>')
jQuery.fn.init [div#d1]


var a = document.createElement('a')
undefined
$(a).attr('href','http://www.baidu.com')
jQuery.fn.init [a]
$(a).text('百度')
jQuery.fn.init [a]
用a替换前面的元素
$('#d1').replaceWith(a)
jQuery.fn.init [div#d1]
           
$(a).replaceAll('#d1')
```

#### clone

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>


<button class="btn">屠龙宝刀,点击就送</button>


<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.js"></script>
<script>
$('.btn').click(function () {
    var btnClone = $(this).clone(true); //
    $(this).after(btnClone);
})

</script>
</body>
</html>
```



今日内容回顾

位置操作

```javascript
offset() 获取相对于整个document的位置,按照左上角来看,
    设置位置:offset({left:200,top:200})
position 

scrollTop 滚轮往下移动了多少了
	用法:$(window).scroll(function(){
        console.log($(window).scrollTop())
    })
	设置值:
		$(window).scrollTop(100) 将滚轮移动到100的位置
scrollLeft
```

尺寸:

```
height  content的高度
width   content的宽度
innerHeight  content + 两个padding
outHeight   content + 两个padding + 两个border

```

文本操作

```
.html()  获取标签和文本
.text()  获取文本
.html('xxx')  识别标签
.text('xxx')
```

值操作

```
val()
	
	input type=text  对象.val()
	input type=radio 选中对象.val()
	select   select标签对象.val()
	
	设置值
		input type=radio 对象.val(['1']) 对应着value属性的值
		select   select标签对象.val('1') 多选照样是数组,
    input type='checkbox' 
    	选中对象.val()  $(':checked') 注意他会将select标签选中的标签也算上,想看多个值需要循环



```

属性

```
设置
attr('age','18')
attr({'age':'18','name':'chao'})
查看
attr('age')
删除
removeAttr('age')

prop()  用在input(type:radio,checkbox),select
prop('checked')  true--false
设置
想设置选中的input标签对象.prop('checked',true) 
取消选中:.prop('checked',false) 


```

文档处理

```
$('div').append(a) 将a添加到div内部的后面
$(a).appendTo('div') 将a添加到div内部的后面

$('div').prepend(a) 将a添加到div内部的前面
$(a).prependTo('div') 将a添加到div内部的前面

after
before
替换
$(a).replaceWith(p)
$(a).replaceAll(p)

```

清空和删除

```
empty() 清空标签中的所有内容,但是标签还在
remove() 删除整个标签
```

克隆clone

```
.clone()  
带事件克隆
.clone(true)
```

事件:

绑定事件的两种方式

```
标签对象.click(function(){})
标签对象.on('click',function(){})
input事件只能用on绑定
```

常用事件

```
click
hover
	对象.hover(
		1.鼠标进入
		function(){},
		2.鼠标移出
		function(){}
	)
blur
focus
change
mouseenter
mouseover
	mouseover 和 mouseenter的区别是：mouseover事件是如果该标签有子标签，那么移动到该标签或者移动到子标签时会连续触发，mmouseenter事件不管有没有子标签都只触发一次，表示鼠标进入这个对象
keyup,keydown
$(window).keyup(function(e){
	e.keyCode;  e为事件对象,keyCode是获取用户按下了哪个键,数字表示
})

```





















































