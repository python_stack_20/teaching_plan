
import os
from NBcrm import settings
from nbapp import form
from django.shortcuts import render,HttpResponse,redirect
from nbapp import models
from django.http import JsonResponse
from django.urls import reverse
from django.contrib import auth
from nbapp import page
from django.contrib.auth.decorators import login_required
from django.views import View
from django.db.models import Q,F,Min,Max,Avg,Count,Sum
# Create your views here.

#注册

def register(request):
    if request.method == 'GET':
        form_obj = form.UserForm()

        return render(request,'register.html',{'form_obj':form_obj})
    else:
        form_obj = form.UserForm(request.POST) #{'username':'chao','password'....}
        if form_obj.is_valid():
            data = form_obj.cleaned_data
            data.pop('r_password')
            models.UserInfo.objects.create_user(**data)
            # auth.login(request,user_obj) 注册完之后可以自动进行登录
            return redirect('login')
        else:
            return render(request, 'register.html', {'form_obj': form_obj})

#登录
def login(request):
    '''
    响应的状态码:
    成功:1000
    验证码失败:1001
    用户名密码失败:1002
    :param request:
    :return:
    '''
    response_msg = {'code':None,'msg':None}
    if request.method == 'GET':
        return render(request, 'login.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        valid_bode = request.POST.get('valid_code')
        #1 首先验证验证码是否正确
        if valid_bode.upper() == request.session.get('valid_str').upper():
            # 2 验证用户名和密码是不是存在
            user_obj = auth.authenticate(username=username,password=password)
            #用户名密码正确
            if user_obj:
                # 3 保存session
                auth.login(request,user_obj)
                response_msg['code'] = 1000
                response_msg['msg'] = '登录成功!'
            else:
                response_msg['code'] = 1002
                response_msg['msg'] = '用户名或者密码输入有误!'

        #验证码失败报错
        else:
            response_msg['code'] = 1001
            response_msg['msg'] = '验证码输入有误!'

        return JsonResponse(response_msg)

#注销
def logout(request):
    auth.logout(request)
    return redirect('login')


def get_valid_img(request):
    import random
    def get_random_color():
        return (random.randint(0,255),random.randint(0,255),random.randint(0,255))
    from PIL import Image,ImageDraw,ImageFont
    img_obj = Image.new('RGB', (200, 34), get_random_color())
    draw_obj = ImageDraw.Draw(img_obj)
    font_path = os.path.join(settings.BASE_DIR,'statics/font/NAUERT__.TTF')
    font_obj = ImageFont.truetype(font_path,16)
    sum_str = ''
    for i in range(6):
        a = random.choice([str(random.randint(0,9)), chr(random.randint(97,122)), chr(random.randint(65,90))])  #4  a  5  D  6  S
        sum_str += a
    print(sum_str)
    draw_obj.text((64,10),sum_str,fill=get_random_color(),font=font_obj)

    width=200
    height=34
    for i in range(5):
        x1=random.randint(0,width)
        x2=random.randint(0,width)
        y1=random.randint(0,height)
        y2=random.randint(0,height)
        draw_obj.line((x1,y1,x2,y2),fill=get_random_color())
    # # 添加噪点
    for i in range(10):
        #这是添加点，50个点
        draw_obj.point([random.randint(0, width), random.randint(0, height)], fill=get_random_color())
        #下面是添加很小的弧线，看上去类似于一个点，50个小弧线
        x = random.randint(0, width)
        y = random.randint(0, height)
        draw_obj.arc((x, y, x + 4, y + 4), 0, 90, fill=get_random_color())

    from io import BytesIO
    f = BytesIO()
    img_obj.save(f,'png')
    data = f.getvalue()

    #验证码对应的数据保存到session里面
    request.session['valid_str'] = sum_str

    return HttpResponse(data)


# 如果记录用户想去的路径
# @login_required
# def shopping(request):
#     path = request.path
#     # return redirect('/login/'+'?next='+path)
#     # request.GET.get('next')
#     # return HttpResponse('shopping')

#这是一个简答的路由跳转测试
# @login_required
# def shopping(request):
#     return HttpResponse('shopping')


#首页
@login_required
def index(request):
    # return render(request,'index.html')

    return render(request,'index.html')

#获取公户所有客户信息
def customers(request):

    current_page_num = request.GET.get('page',1)
    all_customers = models.Customer.objects.filter(consultant__isnull=True)

    per_page_counts = 5
    page_number = 11
    total_count = all_customers.count()

    page_obj = page.PageNation(request.path,current_page_num,total_count,per_page_counts,page_number)

    all_customers = all_customers.order_by('-pk')[page_obj.start_num:page_obj.end_num]

    ret_html = page_obj.page_html()

    return render(request,'customers.html',{'all_customers':all_customers,'ret_html':ret_html})




class AddCustomer(View):
    #获取添加页面
    def get(self,request):
        form_obj = form.CustomerModelForm()
        return render(request,'addcustomer.html',{'form_obj':form_obj})
    def post(self,request):
        form_obj = form.CustomerModelForm(request.POST)
        #{'qq':'11111','name':'xiaohei'}
        if form_obj.is_valid():
            form_obj.save()
            return redirect('customers')

        else:
            return render(request,'addcustomer.html',{'form_obj':form_obj})

  #获取编辑页面
class EditCustomer(View):
    def get(self,request,pk):
        custome_obj = models.Customer.objects.filter(pk=pk).first()
        form_obj = form.CustomerModelForm(instance=custome_obj)

        return render(request,'editcustomer.html',{'form_obj':form_obj})

    def post(self,request,pk):
        custome_obj = models.Customer.objects.filter(pk=pk).first()
        form_obj = form.CustomerModelForm(request.POST,instance=custome_obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect('customers')

        else:
            return render(request,'editcustomer.html',{'form_obj':form_obj})

#删除单条公共客户信息
class DeleteCustomer(View):
    def get(self,request,pk):
        models.Customer.objects.filter(pk=pk).delete()
        return redirect('customers')



#展示私有客户信息
def mycustomers(request):

    my_customers = models.Customer.objects.filter(consultant=request.user)
    return render(request, 'mycustomers.html', {'my_customers': my_customers})




def test(request):
    wd = request.GET.get('wd','')
    condition = request.GET.get('condition','')
    print(wd) #小
    print(condition+'__contains') #name
    condition = condition+'__contains'
    #condition: name
    current_page_num = request.GET.get('page', 1)

    # get:condition=qq&wd=1&page=2

    #<QueryDict: {'condition': ['qq'], 'wd': ['1'], 'page': ['2']}> #condition=qq&wd=1&page=2

    if wd:
        # all_data = models.Customer.objects.filter(Q(qq__contains=wd)|Q(name__contains=wd))
        q = Q()
        q.connector = 'or'  #指定条件连接符号
        q.children.append((condition,wd))  #默认是and的关系
        q.children.append(('qq_name__contains','小'))

        # all_data = models.Customer.objects.filter(name__contains=wd)
        all_data = models.Customer.objects.filter(q)
    else:
        all_data = models.Customer.objects.all()

    per_page_counts = 10 #每页显示10条
    page_number = 7  #总共显示5个页码

    total_count = all_data.count()

    # ret_html,start_num,end_num = page.pagenation(request.path, current_page_num,total_count,per_page_counts,page_number)
    p_obj = page.PageNation(request.path, current_page_num,total_count,request,per_page_counts,page_number)

    ret_html = p_obj.page_html()

    all_data = all_data[p_obj.start_num:p_obj.end_num]

    return render(request,'test.html',{'all_data':all_data,'ret_html':ret_html})










