from django.shortcuts import render,HttpResponse,redirect
from app01 import models
from django.urls import reverse
# Create your views here.

def show_books(request):
    if request.method == 'GET':

        all_book_objs = models.Book.objects.all()


        return render(request,'show_books.html',{'all_book_objs':all_book_objs})


def add_book(request):
    if request.method == 'GET':

        return render(request,'add_book.html')

    else:
        print(request.POST)
        # name = request.POST.get('name')
        # price = request.POST.get('price')
        # date = request.POST.get('date')
        # publisher = request.POST.get('publisher')
        book_info_dict = request.POST.dict()
        #{name': 'python全栈', 'price': '30', 'date': '2019-05-18', 'publisher': '红浪漫'}
        del book_info_dict['csrfmiddlewaretoken']
        print(book_info_dict)
        #打伞
        #**{'name': '九阴真经', 'price': '20', 'date': '2019-05-23', 'publisher': '20期出版社'}
        created_obj = models.Book.objects.create(**book_info_dict)

        # models.Book.objects.create(
        #     name=name,
        #     price=price,
        #     date=date,
        #     publisher=publisher,
        # )


        return redirect('show_books')


def del_book(request,n):

    models.Book.objects.filter(pk=n).delete()
    return redirect('show_books')

def edit_book(request,n):
    if request.method == 'GET':
        old_obj = models.Book.objects.get(pk=n)

        return render(request,'edit_book.html',{'old_obj':old_obj})
    else:

        book_info_dict = request.POST.dict()

        del book_info_dict['csrfmiddlewaretoken']
        print(book_info_dict)
        models.Book.objects.filter(pk=n).update(**book_info_dict)

        return redirect('show_books')