
from rbac import models


def initial_session(request,user_obj):
    permissions = models.Permission.objects.filter(role__user__name=user_obj).distinct()
    # print(permissions)

    permisson_list = []
    permisson_menu_list = []
    for item in permissions:
        # permisson_list = [i.url for i in permissions]
        permisson_list.append(item.url)
        #往session中注入菜单栏的数据

        if item.is_menu:
            permisson_menu_list.append({
                'title':item.title,
                'url':item.url,
                'icon':item.icon,
            })

    '''
        [
            {'title':'客户列表','url':'/customer/list/','icon':'fa-..'},
            {'title':'客户列表','url':'/customer/list/','icon':'fa-..'},
        ]
    
    '''

    print(permisson_list)
    request.session['permisson_list'] = permisson_list
    request.session['permisson_menu_list'] = permisson_menu_list



