from django import template

register = template.Library()

from django.conf import settings
import re


@register.inclusion_tag('rbac/menu.html')
def menu(request):
    menu_list = request.session.get('permisson_menu_list')
    for item in menu_list:
        # url =
        if re.match('^{}$'.format(item['url']), request.path):
            item['class'] = 'active'
            break


    return {"menu_list": menu_list}
