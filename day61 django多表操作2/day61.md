# 昨日内容回顾

多表操作

一对一,一对多,多对多



一对一:

```python
增删改查

​	model.Author.objects.create(name='二哥',age=59,ad_id=1)

删除

​	model.Author.objects.filter(id=1).delete()

改

​	model.Author.objects.filter(id=1).update(name='2ge2')

查

​	正向:

​		model.Author.objects.filter(id=1)[0].ad.telephone

​	反向

​		ad_obj =	model.AuthorDetail.objects.filter(telephone=111)[0]

​		ad_obj.author.id
```



一对多:

​	

​           补充:

```python
	增加:

​		model.Book.objects.create(title='xx',price=12,publish_id=1)

​		model.Book.objects.create(title='xx',price=12,publish=publish的model对象)

​	删除

​		model.Book.objects.filter(id=3)[0].delete()

​		model.Book.objects.filter(id=3).delete()

​	修改:

​		model.Book.objects.filter(id=3).update(title='xxx',publish_id=2)

​	查询

​		反向查询:红浪漫出版社出版过哪些书籍(只要名称)

​			pub_obj = model.Publish.objects.filter(name='红浪漫出版社').first()

​			pub_obj. book_set.all().value()    .value_list() 

​			 .filter(xxx=xxx)  

    author_obj = models.Author.objects.get(name='二哥')
    print(author_obj.book_set.all().values('title').filter(title='linux')) #	<QuerySet [{'title': 'linux'}, {'title': 'linux'}]>
​		正向查询:

​			book_obj = models.Book.objects.filter(title='jinpinmei').first()

​            book_obj .publish.name
```



多对多的

​	增加

```python
	book_obj = models.Book.objects.create(title='水浒',price=11,publish_id=1)
    authorerge = models.Author.objects.get(name='二哥')
    authorsange = models.Author.objects.get(name='三哥')
    book_obj.authors.add(*[authorerge,authorsange])
    book_obj.authors.add(*[authorerge.id,authorsange.id])
    
```

​	删除

```
    book_obj.authors.remove(authorerge)
    book_obj.authors.remove(1)
    book_obj.authors.remove(1,2,3)
    book_obj.authors.remove(*[1,2,3])
    book_obj.authors.clear()
    book_obj.authors.set(['1','2',])
    
```

​	查询:

​		正向查询:

```
   book_obj = models.Book.objects.get(title='水浒')
   book_obj.authors.all().values('name'),.values_list('name')
   book_obj.authors.filter()
   
```

​	反向查询

```
	authorsange = models.Author.objects.get(name='三哥')
	authorsange.book_set.all().values('title')
```



今日内容

​	基于双下划线的跨表查询(连表,正向反向连表的时候怎么写)

​				

​	聚合查询

​		aggregate(Avg('xxx'))   ---  {}

​	分组查询

​		annotate(a = Avg('xxx'))   ---  queryset   

​	F与Q查询

​		F  针对的是单表里面某些字段的批量操作

​		Q  Q(xxx)| Q(ssss)&Q(xss)|~Q(xxxx)   ,filter(Q(Q(xxx)|Q(xxxx))|Q(xxssd),title='xxxx')

























