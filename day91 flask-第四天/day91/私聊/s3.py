# 客户端
# 服务端
# Http  Flask 浏览器
# Websocket GeventWebsocket+Flask 客户端JavaScript（Websocket客户端）
import json

from flask import Flask,request,render_template
from geventwebsocket.handler import WebSocketHandler  # 提供WS协议处理
from geventwebsocket.server import WSGIServer  # 承载服务
from geventwebsocket.websocket import WebSocket  # 语法提示

app = Flask(__name__)

# user_socket_dict = {nicheng:lianjie}
user_socket_dict = {}

@app.route("/my_socket/<username>")
def my_socket(username):
    # 获取当前客户端与服务器的Socket连接
    user_socket = request.environ.get("wsgi.websocket") # type:WebSocket
    if user_socket:
        user_socket_dict[username] = user_socket
        print(len(user_socket_dict),user_socket_dict)
    # print(user_socket,"OK 连接已经建立好了，接下来发消息吧")
    while 1:
        msg = user_socket.receive()
        msg_dict = json.loads(msg)
        # msg = user_socket.receive()
        to_user_nick = msg_dict.get("to_user")
        print(to_user_nick)
        to_user_socket = user_socket_dict.get(to_user_nick)
        to_user_socket.send(msg)




@app.route("/sl")
def sl():
    return render_template("sl.html")



if __name__ == '__main__':
    # app.run("0.0.0.0",9527)
    http_serv = WSGIServer(("0.0.0.0",9527),app,handler_class=WebSocketHandler)
    http_serv.serve_forever()