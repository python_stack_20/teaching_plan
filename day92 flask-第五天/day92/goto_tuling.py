import requests


def go_tl(Q):
    data = {
        "perception": {
            "inputText": {
                "text": Q
            }
        },
        "userInfo": {
            "apiKey": "2213889293634c759484cac88a91c170",
            "userId": "123"
        }
    }


    res = requests.post("http://openapi.tuling123.com/openapi/api/v2",json=data)

    res_dict = res.json()

    return res_dict.get("results")[0].get("values").get("text")
