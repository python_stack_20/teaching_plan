import re
from django.shortcuts import HttpResponse,render,redirect
from django.utils.deprecation import MiddlewareMixin
from rbac import models

class PermissonMiddleWare(MiddlewareMixin):

    def process_request(self,request):
        #白名单放行
        for i in ['/login/','/admin/.*']:
            ret = re.search(i,request.path)
            if ret:
                return None

        #登录认证
        user = request.session.get('user')
        if not user:
            return redirect('login')



        request.menu_breadcrumb = [
            {'title':'首页','url':'javascript:void(0);'},
            # {'title':'首页1','url':'javascript:void(0);'},
            # {'title':'首页2','url':'javascript:void(0);'},

        ]

        #权限认证
        for item in request.session['permisson_list']:

            # [
            #     {'url':'/customer/add/','pid':'1','pk':1}
            #
            # ]

            reg = '^%s$'%item['url']
            ret = re.search(reg,request.path)
            if ret:

                request.show_id = item['pid']
                if item['pid'] == item['pk']:
                    request.menu_breadcrumb.append({
                        'title': item['title'],
                        'url': request.path,
                    })
                else:
                    obj = models.Permission.objects.filter(pk=item['pid']).first()
                    l1 = [
                        {  #父权限  客户列表
                            'title':obj.title,
                            'url':obj.url
                        },
                        {  # 子权限  客户列表
                            'title': item['title'],
                            'url': request.path,
                        }

                    ]
                    request.menu_breadcrumb.extend(l1)
                # request.show_id = item['pk']



                return None
        else:
            return HttpResponse('不好意思,权限不够!!无权访问!')


'''
    1.哪些权限在菜单栏显示,设计数据表,一级菜单表menu,权限表加了一个menu字段,foreignkey到menu表,权限注入,设计数据结构:
    
    {
            1:{  # 1--menu_id
                'title':'..',
                'icon':'xx',
                children:[
                    {'title':'二级','url':'xx'},
                    {'title':'二级','url':'xx'},
                ]
            },
            2:{  # 2--menu_id
                'title':'..',
                'icon':'xx',
                children:[
                    {'title':'二级','url':'xx'},
                    {'title':'二级','url':'xx'},
                ]
            }
            
        }
    

'''







