
# 导包
# 1, 标准库
# 2, 第三方
# 3, 自己/自定义
# 变量赋值, 逗号, : 都需要空空格
# 文件尾行空一行空行即可
# 更多 pep8 规范
# TODO 我做了一件什么事情 没做完 ?
# FIXME 这里没有做极值的判断, 可能会出现 bug

# import copy

from django.shortcuts import render
from django.shortcuts import reverse
from django.shortcuts import redirect

# from django.shortcuts import (
#     render, redirect, reverse
# )

from rbac import models
# from rbac.server.init_permission import init_permission
from rbac.service.permission_input import initial_session


def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        pwd = request.POST.get('pwd')
        user = models.User.objects.filter(name=username, password=pwd).first()
        
        if not user:
            err_msg = '用户名或密码错误'
            return render(request, 'login.html', {'err_msg': err_msg})

        # 将用户信息注入到session中
        request.session['user'] = user.name
        # 登录成功
        # 将权限信息写入到session
        # init_permission(request,user)
        initial_session(request, user)
        # 发送推广信息
        # 给某某类发什么信息
        #
        return redirect(reverse('customer'))

    return render(request, 'login.html')
