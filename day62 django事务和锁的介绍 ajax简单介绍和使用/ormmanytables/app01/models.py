from django.db import models

# Create your models here.


class Author(models.Model):
    name=models.CharField( max_length=32)
    age=models.IntegerField()
    ad = models.OneToOneField(to='AuthorDetail')
    def __str__(self):
        return self.name

class AuthorDetail(models.Model):
    birthday=models.DateField()
    telephone=models.BigIntegerField()
    addr=models.CharField( max_length=64)

class Publish(models.Model):
    name=models.CharField( max_length=32)
    city=models.CharField( max_length=32)

class Book(models.Model):
    title = models.CharField( max_length=32)
    price=models.DecimalField(max_digits=5,decimal_places=2)
    zan = models.IntegerField(default=10)
    comment = models.IntegerField(default=10)
    publishs=models.ForeignKey(to="Publish",related_name='bp1')
    authors=models.ManyToManyField(to='Author',)
    def __str__(self):
        return self.title












