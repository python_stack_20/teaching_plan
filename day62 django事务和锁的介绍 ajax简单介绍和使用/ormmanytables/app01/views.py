from django.shortcuts import render,HttpResponse,redirect
from app01 import models
from django.db.models import Avg,Min,Max,Count,F,Q

# Create your views here.

def query(request):

    # 多对一添加记录
    # pubklish_obj = models.Publish.objects.filter(id=2)[0]
    # models.Book.objects.create(
    #     title='linux',
    #     price=100,
    #     # publish=pubklish_obj, #方式1
    #     publish_id=2,  #方式2:这种方式来添加数据用的比较多
    # )
    #删除记录和更新记录和单表都是一样的
    # models.Book.objects.filter(id=1).delete()
    # models.Book.objects.filter(id=2).update(price=998)


    #一对一增加
    # models.Author.objects.create(
    #     name='二哥',
    #     age=59,
    #     # ad=models.AuthorDetail.objects.get(id=1),
    #     ad_id=2,
    # )
    #更新和删除
    # models.Author.objects.filter(id=3).update(age=66)
    # models.Author.objects.filter(id=3).delete()

    # 多对多的增删改

    # 增加:
    # book_obj = models.Book.objects.create(
    #     title='linux',
    #     price=100,
    #     publish_id=2,
    # )
    # # [1,3]
    # author1 = models.Author.objects.get(id=1)
    # author3 = models.Author.objects.get(id=3)
    # book_obj = models.Book.objects.get(id=3)
    # book_obj.authors.add(author1,author3)
    # book_obj.authors.add(1,3)
    # book_obj.authors.add(*[1,3]) #用的比较多
    #删除
    # book_obj.authors.remove(1)
    # book_obj.authors.remove(1,2)
    # book_obj.authors.remove(*[1,2])
    # book_obj.authors.clear()  #全部清除
    # book_obj.authors.set(['3','7'])  #清空所有的book_id为3的第三张表里面的所有记录,再重新写上book_id为3的对应的author_id为1的关系记录


    #一对一  一对多  多对多
    # 一对多:
    # 正向查询,查询主键为2的书籍的出版社所在的城市
    # book_obj = models.Book.objects.get(id=2)
    # ret = book_obj.publish.city  #book_obj.属性.属性名称
    # # print(book_obj.publish.__dict__) #Publish object
    # print(book_obj.publish) #Publish object
    # print(ret) #昌平
    #
    # # # 反向查询的:红浪漫出版社出版过哪些书籍
    # publish_obj = models.Publish.objects.filter(name='红浪漫出版社').first()
    # book_list = publish_obj.book_set.all()  #类名小写_set
    # print(book_list)
    # print(book_list) #<QuerySet [<Book: linux>, <Book: linux>, <Book: c>]>

    # print(models.Book.objects.filter(id=3))
    # print(publish_obj.book_set.all().filter(id=3))

    #一对一的
    #正向查询:三哥是哪个城市的,
    # author_obj = models.Author.objects.get(name='三哥')
    # addr_info = author_obj.ad.addr
    # print(addr_info) #于辛庄
    # #反向查询:看一下谁在松兰堡
    # ad_obj = models.AuthorDetail.objects.get(addr='松兰堡')
    # # publish_obj.book_set.all()
    # print(ad_obj.author.name) #二哥

    #多对多
    #正向查询的: python的作者是谁
    # book_obj = models.Book.objects.filter(title='python').first().authors.all()
    # # authors_list = book_obj.authors.all() #<QuerySet [<Author: 三哥>, <Author: 二哥>, <Author: B哥>]>
    # print(book_obj)

    #反向查询:二哥写了哪些书
    # author_obj = models.Author.objects.get(name='二哥')
    # # print(author_obj.book_set.all().values('title').filter(title='linux')) #<QuerySet [{'title': 'linux'}, {'title': 'linux'}]>
    # book_obj = models.Book.objects.filter(id=3).first()
    # book_obj.authors.remove(author_obj)

    #查询主键为2的书籍的出版社所在的城市
    # ret = models.Book.objects.filter(id=2).values('publishs__city') #<QuerySet [{'publish__city': '昌平'}]>
    #
    # print(ret)
    # ret = models.Publish.objects.filter(book__id=2).values('city','book__title')
    #
    # print(ret)
    #三哥是哪个城市的
    # ret = models.Author.objects.filter(name='三哥').values().filter(name='三哥')
    # print(ret) #<QuerySet [{'ad__addr': '于辛庄'}]>
    # ret = models.AuthorDetail.objects.filter(author__name='三哥').values('author__age') #<QuerySet [{'id': 1, 'birthday': datetime.date(2019, 5, 1), 'telephone': 120, 'addr': '于辛庄'}]>
    # print(ret) #<QuerySet [{'addr': '于辛庄'}]>

    # 多对多
    # python的作者是谁   title=python
    # ret = models.Book.objects.filter(title='python').values('authors__name')
    # print(ret) #<QuerySet [{'authors__name': '三哥'}, {'authors__name': '二哥'}, {'authors__name': 'B哥'}]>
    #
    # ret = models.Author.objects.filter(book__title='python').values('name')
    #
    # print(ret)

    # 查询红浪漫出版社出版过的所有书籍的名字以及作者的姓名
    # ret = models.Publish.objects.filter(name='红浪漫出版社').values('book__title','book__authors__name')
    #<QuerySet [{'book__title': 'linux', 'book__authors__name': '二哥'}, {'book__title': 'linux', 'book__authors__name': 'B哥'}, {'book__title': 'linux', 'book__authors__name': '雄哥'}, {'book__title': 'c', 'book__authors__name': None}]>


    # print(ret)
    #手机号大于115的作者出版过的所有书籍名称以及出版社名称
    # ret = models.AuthorDetail.objects.filter(telephone__gt=115).values('author__book__title','author__book__publishs__name')

    #<QuerySet [{'author__book__title': 'python', 'author__book__publishs__name': '20期出版社'}, {'author__book__title': 'python', 'author__book__publishs__name': '20期出版社'}, {'author__book__title': 'linux', 'author__book__publishs__name': '红浪漫出版社'}]>
    # print(ret)

    # related_name: 反向查询的:红浪漫出版社出版过哪些书籍
    # publish_obj = models.Publish.objects.filter(name='红浪漫出版社').first()
    # book_list = publish_obj.book_set.all()  # 类名小写_set
    # print(book_list)
    #

    ##################聚合查询#######################

    # ret = models.Book.objects.all().aggregate(avgPirce = Avg('price'))
    # print(ret) #{'price__avg': 165.892857} {'avgPirce': 165.892857}
    # ret = models.Book.objects.all().aggregate(Max('price'))
    # print(ret)  # {'price__avg': 165.892857}

    #################F查询######################
    # models.Book.objects.filter(price__gt=50).update(price=F('price')-20)

    # ret = models.Book.objects.filter(comment__gt=F('zan')) #<QuerySet [<Book: linux>, <Book: java>]>
    # print(ret)
    #################Q查询######################
    # ret = models.Book.objects.filter(Q(price__gt=50)&Q(comment__gt=20))
    # ret = models.Book.objects.filter(Q(price__gt=50)|Q(comment__gt=20))
    # ret = models.Book.objects.filter(Q(price__gt=50)|~Q(comment__gt=20))
    # print(ret) #<QuerySet [<Book: linux>, <Book: linux>, <Book: java>, <Book: c>]>

#############分组查询##################

    #单表的   查询出版社的id和每个出版过的书籍的平均价格
    # ret = models.Book.objects.values('publishs_id','zan').annotate(a=Avg('price')) #<QuerySet

   #多表的    查询出版社的名称和每个出版过的书籍的最高价
    # ret = models.Book.objects.values('publishs__name').annotate(m=Max('price'))
    # print(ret)

    # ret = models.Publish.objects.values('name').annotate(m=Max('bp1__price'))
    # print(ret)
    #
    # ret = models.Publish.objects.annotate(m=Max('bp1__price')).values('name','bp1__price')
    # print(ret)

    # ret = models.Author.objects.annotate(a=Max('book__price')).values('book__title','a','name')

    #raw()方法执行原生sql语句

    # ret = models.Publish.objects.raw("select id,title from app01_book;")
    # print(ret)
    # for i in ret:
    #     print(i.title)
    from django.db import connection
    # conn = pymysql(xxxx)
    cursor = connection.cursor()
    sql = "select title from app01_book;"
    cursor.execute(sql)
    ret = cursor.fetchall()
    print(ret)
    #<QuerySet [{'book__title': 'python', 'name': '三哥', 'a': Decimal('11.00')}, {'book__title': 'linux', 'name': '二哥', 'a': Decimal('979.00')}, {'book__title': 'python', 'name': '二哥', 'a': Decimal('11.00')}, {'book__title': 'linux', 'name': 'B哥', 'a': Decimal('979.00')}, {'book__title': 'python', 'name': 'B哥', 'a': Decimal('11.00')}, {'book__title': 'linux', 'name': '雄哥', 'a': Decimal('81.00')}, {'book__title': 'Py', 'name': '雄哥', 'a': Decimal('21.00')}]>
    return HttpResponse('ok')


