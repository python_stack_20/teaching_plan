


if __name__ == '__main__':
    import os
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ormmanytables.settings")
    import django
    django.setup()

    from app01 import models
    ret = models.Book.objects.all().values('title')
    print(ret)

