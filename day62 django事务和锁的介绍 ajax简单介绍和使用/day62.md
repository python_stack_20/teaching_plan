# 昨日内容回顾

基于双下划线的多表查询

```python
一对一
	查一下雄哥的手机号
models.Author.objects.filter(name='雄哥').values('ad__telephone')
models.AuthorDetail.objects.filter(author__name='雄哥').values('telephone')

一对多
	红浪漫出版了哪些书
models.Publish.objects.filter(name='红浪漫出版社').values('book__title')
models.Book.objects.fitler(publishs__name='红浪漫出版社').values('title')

多对多
	python这本书是哪些作者写的
models.Book.objects.filter(title='python').values('authors__name')
models.Author.objects.filter(book__title='python').values('name')


```



聚合

```python
查询一下所有书的平均价格
from django.db.models import Avg,Max,Min,Count,Sum
models.Book.objects.all().aggregate(a=Avg('price')) #结果是个字典
```

分组

```python
查询作者出版过的书的平均价格
models.Author.objects.values('name').annotate(a=Avg('book__price'))
models.Book.objects.values('authors__name').annotate(a=Avg('price'))

```

F

```python
我先查看点赞数小于评论数的所有书籍
from django.db.models import F
model.Book.objects.filter(zan__lt=F('comment'))
统一给价格加100块
model.Book.objects.all().update(price=F('price')+100)

```

Q

```python
查询2019年的点赞数小于100的,或者评论小于100的书籍名称和价格
models.Book.objects.filter(Q(zan__lt=100)|Q(comment__lt=100),date__year=2019).values('title','price')
```



作业:

```
#1 查询每个作者的姓名以及出版的书的最高价格
ret = models.Author.objects.annotate(m=Max('book__price')).values('name','m')
ret = models.Book.objects.values('authors__name').annotate(m=Max('price'))
print(ret)

#2 查询作者id大于2作者的姓名以及出版的书的最高价格
models.Author.objects.filter(id__gt=2).annotate(m=Max('book__price')).values('name','m')

#3 查询作者id大于2或者作者年龄大于等于20岁的,女作者的姓名以及出版的书的最高价格
models.Author.objects.filter(Q(sex='女')&Q(Q(id__gt=2)|Q(age__gte=20))).values('name').annotate(m=Max('book__price'))
#4 查询每个作者出版的书的最高价格 的平均值
models.Author.objects.values('id').annotate(a=Max('book__price')).aggregate(m=Avg('a'))
#5 每个作者出版的所有书的最高价格的那本书的名称
models.Author.objects.values('name').annotate(a=Max('book__price')).values('book__title')



```



今日内容

```
锁,事务

ajax

cookie和session
```



今日的作业:

```

```



今日内容回顾

```
事务  
全局  settings配置

局部
	from django.db import transaction
	视图函数装饰器的形式
		@transaction.atomic
		def view()...
    上下文的:
    	with transaction.atomic():
    		sql.....
    		
select_for_update()
select * from t1  for update
models.T1.objects.select_for_update().filter(id=1)  #排他锁(互斥锁)  读写不行写也不行
```



ajax

```
异步发请求
局部刷新
jquery版的:
$.ajax({
	url:'目标地址',
	type:'get',
	
	success:function(response){
		console.log(response)
		各种dom和bom操作
		
		
	}

})


```





