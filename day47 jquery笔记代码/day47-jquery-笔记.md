# jquery

#### 一.引入方式

```javascript
<script src="jquery-3.4.1.js"></script>
<script>
	jQuery代码
	原生js可以写
</script>

```



#### 二.jQuery和dom对象的转换方法:

![1557283736408](C:\Users\lucky\AppData\Roaming\Typora\typora-user-images\1557283736408.png)

```javascript
var $variable = jQuery对像
var variable = DOM对象
$variable[0]//jQuery对象转成DOM对象，通过一个jQuery对象+[0]索引零，就变成了DOM对象，就可以使用JS的代码方法了，DOM对象转换成jQuery对象：$(DOM对象)，通过$符号包裹一下就可以了
```

## 三.选择器

基本选择器

![1557285402788](C:\Users\lucky\AppData\Roaming\Typora\typora-user-images\1557285402788.png)

```javascript
基本筛选器:
$('li:even')
jQuery.fn.init(3) [li, li, li, prevObject: jQuery.fn.init(1)]0: li1: li2: lilength: 3prevObject: jQuery.fn.init [document]__proto__: Object(0)
$('li:odd')
jQuery.fn.init(3) [li#l2, li#l4, li, prevObject: jQuery.fn.init(1)]
```

属性选择器

![1557287633407](C:\Users\lucky\AppData\Roaming\Typora\typora-user-images\1557287633407.png)



![1557288461870](C:\Users\lucky\AppData\Roaming\Typora\typora-user-images\1557288461870.png)

父级选择器

```javascript
$('span')
jQuery.fn.init [span.c4, prevObject: jQuery.fn.init(1)]0: span.c4length: 1prevObject: jQuery.fn.init [document]__proto__: Object(0)
$('span').parent()
jQuery.fn.init [div.c3, prevObject: jQuery.fn.init(1)]0: div.c3length: 1prevObject: jQuery.fn.init [span.c4, prevObject: jQuery.fn.init(1)]__proto__: Object(0)
$('span').parents()
jQuery.fn.init(4) [div.c3, div.c2, body, html, prevObject: jQuery.fn.init(1)]0: div.c31: div.c22: body3: htmllength: 4prevObject: jQuery.fn.init [span.c4, prevObject: jQuery.fn.init(1)]__proto__: Object(0)
$('span').parentsUntil('body')
jQuery.fn.init(2) [div.c3, div.c2, prevObject: jQuery.fn.init(1)]0: div.c31: div.c2length: 2prevObject: jQuery.fn.init [span.c4, prevObject: jQuery.fn.init(1)]__proto__: Object(0)
```

绑定点击事件

```javascript
    // 标签对象.onclick = function
    $('.c1').click(function () {
        // this是dom对象
        $(this).css('background-color','red');
    })
```



## 今日内容回顾

### 	jQuery的两种引入方式

​		本地文件引入
​		CDN引入(网络资源引入)

#### 	DOM对象和jquery对象的互相转换

```javascript
dom---jquery  $(dom)
jquery--dom   jquery对象[0]
```

#### 选择器

##### 基本选择器

```
ID选择器:$('#id值')
类选择器:$('.类值')
标签选择器:$('标签名')
```

层级选择器

```
$('div p') 找后代
...
```

基本筛选器

```
$('div:first')
:last
:eq(索引)
:has(选择器)
:not(选择器)   
:even 索引为偶数的
:odd  索引为奇数的
:gt(索引) 大于
:lt(索引) 小于
```

属性选择器

```
[属性]
[属性 = '值']
[属性 != '值']
input[type='text']
```

表单筛选器

```
:text    $(':text')
...
```

表单属性

```
:disabled $(':disabled')
:checked
:selected
:enabled
```

筛选器方法

```
$('div').find('p')  找到的是p标签,找的是div后代中所有的p标签
$('div p')

filter
$('div').filter('.c1') 找的是带有class=c1的div标签

下一个:  平级找
$('#d1').next()
$('#d1').nextAll()
$('#d1').nextUntil('#d2')
上一个
$('#d1').prev()
$('#d1').prevAll() #注意找到的标签的顺序,倒叙
$('#d1').prevUntil('#d2') 
父亲元素
$('#d1').parent()
$('#d1').parents()
$('#d1').parentsUntil('body')

儿子:
$('#d1').children('#d2') 找所有的:$('#d1').children()
兄弟
$('#d1').siblings('.d2')

$('.d1').first()
$('.d1').last() 
...

```

绑定点击事件

```
$('#d1').click(
	function(){
		...
	}
)
```

