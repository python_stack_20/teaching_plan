from django.shortcuts import render,redirect,HttpResponse

# Create your views here.

def index_xxx(request):
    print('index视图函数')
    # raise ValueError('视图函数出错拉')
    # ret = render(request, 'index.html')
    ret = HttpResponse('ok')

    def render():
        print('我是render函数!!!')
        return HttpResponse("render函数的返回数据")
    ret.render = render
    return ret

def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')

    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        if username == 'chao' and password == '123':
            request.session['is_login'] = True
            request.session['user'] = username

            return redirect('home')
        else:
            return redirect('login')


def home(request):
    return render(request,'home.html')
