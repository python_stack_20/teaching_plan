# 昨日内容回顾

## cookie和session

### cookie

```
由于HTTP协议是无状态的,所以没有办法保持会话,基于这种情况,浏览器发展出了cookie技术,通过cookie就能够完成持续对话,大致过程就是类似一个字典的东西,从客户端浏览器访问服务的时候,带着这个字典,name后端服务器就能够身份认证的标识或者一些其他的信息,放到cookie里面,共客户端使用,并且以后客户端再来访问服务端就会一直带着这个cookie,后端就客户端的认证就可以从cookie里面的数据进行认证,从而完成保持会话的功能.

```



#### django操作cookie的方法

```python
request.COOKIES[key]
request.COOKIES.get(key)
request.COOKIES.get_signed_cookie(key,salt='xxx')
ret = HttpResponse('x')
ret.set_cookie(key,value,)
ret.set_signed_cookie(key,value,)

更新:设置相同的key,request.session[key2] = value2就把之前的覆盖了
```



#### session

```python
设置值
request.session[key1] = value1
request.session[key2] = value2
1.生成一个随机字符串asdf
2.key1:value1,key2:value2 -- 加密,然后放到djangosession表中asdf  加密后的数据  过期日期(14)  ,一个用户对应一个浏览器对应一个服务端,就有一条session记录
3.设置cookie,将随机字符串(那个钥匙)  ret.set_cookie(sessionid,随机字符串,)
查询:
request.session[key1]
request.session.get(key1)

删除:
request.session.flush()


```



# 今日内容回顾

### 中间件

django请求生命周期

自定义中间件

任意位置下创建一个任意名字的py文件,比如叫BGEAINI.py

settings配置

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'app01.BGEAINI.MD1',  #路径
    'app01.BGEAINI.MD2',
    # 'app01.BGEAINI.SessionAuth',

]
```

BGEAINI.py文件中写类

```python
from django.utils.deprecation import MiddlewareMixin
from django.urls import reverse
from django.shortcuts import HttpResponse,redirect
class MD1(MiddlewareMixin): #继承MiddlewareMixin

    def process_request(self,request):  
        print('MD1')

        # return HttpResponse('你的请求有异常')#,None是正常的请求
    def process_view(self, request, view_func, view_args, view_kwargs):
        print('MD1的process_view')
        print(view_func.__name__) #index
        # return HttpResponse('ok')

    def process_response(self,request,response):
        # print(response)
        print('MD1的响应')
        # print(response.__dict__)

        return response  #接力棒
    def process_exception(self, request, exception):
        print('MD1的process_exception')
    def process_template_response(self, request, response):
        print("MD1 中的process_template_response")
        return response
```



中间的五个方法

```python
process_request(self,request)  #请求来的时候,过中间件会自动执行的方法,正常的话,return None,异常的话直接可以回复HttpResponse对象,顺序从上之下执行

process_response(self, request, response) #响应回去会自动执行的方法,注意,return response  顺序从下往上倒序的.任意一个中间件的process_request方法里面如果出现了return一个HttpResponse对象,那么就会不继续执行其他内容了,而是直接执行各个中间件的process_response方法,如果自己的中间件里面有process_response这个方法,会先执行它自己的如果自己的中间件里面有process_response这个方法.直接返回.

process_view(self, request, view_func, view_args, view_kwargs) #url分发之后,视图执行之前,执行,从上到下正序的.

process_exception(self, request, exception) #视图函数报错了,会执行,顺序倒序的
process_template_response(self,request,response)#HttpResponse对象里面必须有个render函数,才会执行process_template_response方法,并且顺序是倒序的.并且render函数里面必须return一个HttpResponse对象,会将之前的覆盖掉

```



















