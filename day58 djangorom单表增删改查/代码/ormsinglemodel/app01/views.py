from django.shortcuts import render,HttpResponse,redirect
from app01 import models
# Create your views here.
import datetime

def show(request):
    #创建数据
    # 方式1:
    # date = datetime.date.today() #2019-05-23
    # print(date,type(date))
    #
    # # book_obj = models.Book(name='三国',price=100,date=date)
    # book_obj = models.Book()
    # book_obj.save()
    # from django.db import connection
    # print(connection.queries)

    #方式二
    # book_obj = models.Book.objects.create(
    #     name='水浒4', price=100, date="2018-12-12"
    # )


    # print(book_obj)
    # print(book_obj.name)
    # print(book_obj.price)

    #查询所有数据的方法
    # all_objs = models.Book.objects.all()
    # print(all_objs)
    #查询条件数据
    # book_obj = models.Book.objects.filter(
    #     # pk=1
    #     id=2
    # )
    # print(book_obj)

    # book_obj = models.Book.objects.filter(price=1000)  #获取单条记录,并且是model对象,并不是queryset
    # book_obj = models.Book.objects.get(price=1000)  #获取单条记录,并且是model对象,并不是queryset
    # book_obj = models.Book.objects.get(pk=3)  #获取单条记录,并且是model对象,并不是queryset
    # get方法获取数据的两种错误,get方法必须获取一条数据
    # 超过一条的报错:get() returned more than one Book -- it returned 3!
    # 没有 :Book matching query does not exist.
    # filter不会报错,但是返回的是QuerySet类型
    # print(book_obj)
    # book_obj.delete()
    # 删除,谁调用,model对象和queryset类型都可以
    # models.Book.objects.filter(pk=1).delete()

    #更新,update,由queryset类型调用,model对象不能调用更新方法
    # models.Book.objects.filter(name='水浒').update(
    #     price=333,date='2019-5-20'
    # )
    # book_obj.update(name='金瓶梅') #报错:'Book' object has no attribute 'update',

    #批量创建
    # obj_list = []
    # for i in range(1,11):
    #     obj = models.Book(
    #         name='太白与狗的故事%s'%i,
    #         price=10+i,
    #         date='2019-5-%s'%i
    #     )
    #     obj_list.append(obj)
    #
    # models.Book.objects.bulk_create(obj_list)

    # update_or_create方法,有就更新,没有就创建

    # obj1,obj2 = models.Book.objects.update_or_create(
    #     name='水浒2', #查询参数
    #     defaults={    #需要更新的值
    #         'price':111,
    #         'date':'2011-11-11'
    #     }
    # )
    # print(obj1,created_status) #水浒2--111 False  返回的更新对象

    # obj1, created_status = models.Book.objects.update_or_create(
    #     name='水浒12',  # 查询参数
    #     defaults={  # 需要更新的值
    #         'price': 222,
    #         'date': '2011-11-11'
    #     }
    # )
    # print(obj1, created_status)  # 水浒12--222 True 新创建的数据,created_status为true,返回的第一个值为新创建的model对象

    # obj1, created_status = models.Book.objects.update_or_create(
    #     price=100,  # 查询参数,如果查询的数据为多条,会报错,因为内部调用的是get方法
    #     #错误信息: get() returned more than one Book -- it returned 2!
    #     defaults={  # 需要更新的值
    #         'name': 'xxx',
    #         'date': '2012-2-2'
    #     }
    # )
    # print(obj1, created_status)  # 水浒12--222 True 新创建的数据,created_status为true,返回的第一个值为新创建的model对象

    return HttpResponse('ok')



def query(request):
    #exclude排除,可以通过objects来调用,还可通过queryset来调用
    # obj_list = models.Book.objects.exclude(id=6)
    # print(obj_list)

    # obj_list = models.Book.objects.filter(price=100).exclude(id=4)
    # print(obj_list)

    #order_by,-号是降序,
    # obj_list = models.Book.objects.all().order_by('-price','id')
    # print(obj_list)
    #reverse反转
    # obj_list = models.Book.objects.all().order_by('-price').reverse()
    # print(obj_list)
    #count统计结果个数
    # obj_count = models.Book.objects.filter(price=100).count()
    # print(obj_count) #2
    # obj_list = models.Book.objects.filter(price=100).first()
    # obj_list = models.Book.objects.filter(price=100)[0]
    # print(obj_list)
     #exists方法
    # obj_list = models.Book.objects.filter(price=1000).exists()
    # print(obj_list) #True

    # obj_list = models.Book.objects.filter(price=1000)
    # if obj_list.exists():

    # obj_list = models.Book.objects.filter(price=100).values('id','name')
    # # for obj in obj_list:
    # #     print(obj.id)
    # #     print(obj.name)
    # print(obj_list) #<QuerySet [{'id': 4, 'name': '水浒3'}, {'id': 5, 'name': '水浒4'}]>

    # obj_list = models.Book.objects.values('id','name')
    # print(obj_list)
    # obj_list = models.Book.objects.values_list('id', 'name')
    # obj_list = models.Book.objects.filter(price=100).values_list('id', 'name')
    # print(obj_list) #<QuerySet [(4, '水浒3'), (5, '水浒4')]>
    # obj_list = models.Book.objects.all().values('price').distinct()
    # print(obj_list) #<QuerySet [{'price': 111}, {'price': 100}, {'price': 100}, {'price': 11}, {'price': 12}, {'price': 13}, {'price': 14}, {'price': 15}, {'price': 16}, {'price': 17}, {'price': 18}, {'price': 19}, {'price': 20}, {'price': 222}]>
    # print(obj_list[0],type(obj_list[0])) #{'price': 111} <class 'dict'>


    # obj_list= models.Book.objects.filter(price__in=[11,12,13])

    # obj_list = models.Book.objects.filter(price__gte=100)

    # obj_list = models.Book.objects.filter(name__contains='水')


    # obj_list = models.Book.objects.filter(date__year='2019',date__month='5',date__day__gt='6')


    # print(obj_list) #<QuerySet [<Book: 太白与狗的故事1--11>, <Book: 太白与狗的故事2--12>, <Book: 太白与狗的故事3--13>]>




    return HttpResponse('ok')


