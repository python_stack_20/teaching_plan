

# 昨日内容回顾

静态文件配置

```

STATIC_URL = '/static/' #别名

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'statics'), #路径配置
]

```



```html
使用

{% load static %}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
{#    <link rel="stylesheet" href="/static/css/indexcss.css">#} 直接用别名
    <link rel="stylesheet" href="{% static 'css/indexcss.css' %}">  用static功能找到别名,通过别名找到路径,然后将路径替换到这里
</head>
<body>

<h1>这是index页面</h1>

<img src="/static/img/1.png" alt="">

<div class="c1"></div>

<script src="/static/js/indexjs.js"></script>
</body>
</html>
```



url反向解析:

```python
html文件中url反向解析的用法
<form action="{% url 'login' %}" method="post">

    {% csrf_token %}
    用户名 <input type="text" name="username">
    <input type="submit">

</form>

view视图里面反向解析的用法
from django.urls import reverse

def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:

        username = request.POST.get('username')
        print(username)
        print(reverse('xx')) #/index/

        return redirect(reverse('xx'))
        return redirect('xx')

```





#### ORM (Object Relational Mapping)

mysql里面通过orm创建 一个表

1 mysql---创建一个库  rom01

2 settings---DATABASES这个配置

```python

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME':'orm01',# 要连接的数据库，连接前需要创建好
        'USER':'root', # 连接数据库的用户名
        'PASSWORD':'123', # 连接数据库的密码
        'HOST':'127.0.0.1',       # 连接主机，默认本级
        'PORT':3306 #  端口 默认3306
    }
}
```



3 和settings同级目录的`__init__.py`,

```
import pymysql

​	pymysql.install_as_MySQLdb()
```



4 models.py

```python
class Book(models.Model):
	id=models.AutoField(primary_key=True)
    ...
```

5执行指令

```
python manage.py makemigrations
python manage.py migrate
```



### orm增删改查

增加:

```
    #创建数据
    # 方式1:
    # date = datetime.date.today() #2019-05-23
    # print(date,type(date))
    #
    # # book_obj = models.Book(name='三国',price=100,date=date)
    # book_obj = models.Book()
    # book_obj.save()
    # from django.db import connection
    # print(connection.queries)
    
    #方式2
    # book_obj = models.Book.objects.create(
    #     name='水浒4', price=100, date="2018-12-12"
    # )
```

简单查询

```python
    #查询所有数据的方法
    # all_objs = models.Book.objects.all()
    # print(all_objs) <QuerySet [<Book: 水浒>.....]>
    #查询条件数据
    # book_obj = models.Book.objects.filter(
    #     # pk=1
    #     id=2
    # )
    # print(book_obj)<QuerySet [<Book: 水浒>]>,queryset类型操作起来类似列表
```



删除:

delete()

其他:

```python
    #创建数据
    # 方式1:
    # date = datetime.date.today() #2019-05-23
    # print(date,type(date))
    #
    # # book_obj = models.Book(name='三国',price=100,date=date)
    # book_obj = models.Book()
    # book_obj.save()
    # from django.db import connection
    # print(connection.queries)

    #方式二
    # book_obj = models.Book.objects.create(
    #     name='水浒4', price=100, date="2018-12-12"
    # )


    # print(book_obj)
    # print(book_obj.name)
    # print(book_obj.price)

    #查询所有数据的方法
    # all_objs = models.Book.objects.all()
    # print(all_objs)
    #查询条件数据
    # book_obj = models.Book.objects.filter(
    #     # pk=1
    #     id=2
    # )
    # print(book_obj)

    # book_obj = models.Book.objects.filter(price=1000)  #获取单条记录,并且是model对象,并不是queryset
    # book_obj = models.Book.objects.get(price=1000)  #获取单条记录,并且是model对象,并不是queryset
    book_obj = models.Book.objects.get(pk=3)  #获取单条记录,并且是model对象,并不是queryset
    # get方法获取数据的两种错误,get方法必须获取一条数据
    # 超过一条的报错:get() returned more than one Book -- it returned 3!
    # 没有 :Book matching query does not exist.
    # filter不会报错,但是返回的是QuerySet类型
    # print(book_obj)
    # book_obj.delete()
    # 删除,谁调用,model对象和queryset类型都可以
    # models.Book.objects.filter(pk=1).delete()

    #更新,update,由queryset类型调用,model对象不能调用更新方法
    # models.Book.objects.filter(name='水浒').update(
    #     price=333,date='2019-5-20'
    # )
    # book_obj.update(name='金瓶梅') #报错:'Book' object has no attribute 'update',
```

批量创建和update_or_create:

```python
#批量创建
    # obj_list = []
    # for i in range(1,11):
    #     obj = models.Book(
    #         name='太白与狗的故事%s'%i,
    #         price=10+i,
    #         date='2019-5-%s'%i
    #     )
    #     obj_list.append(obj)
    #
    # models.Book.objects.bulk_create(obj_list)

    # update_or_create方法,有就更新,没有就创建

    # obj1,obj2 = models.Book.objects.update_or_create(
    #     name='水浒2', #查询参数
    #     defaults={    #需要更新的值
    #         'price':111,
    #         'date':'2011-11-11'
    #     }
    # )
    # print(obj1,created_status) #水浒2--111 False  返回的更新对象

    # obj1, created_status = models.Book.objects.update_or_create(
    #     name='水浒12',  # 查询参数
    #     defaults={  # 需要更新的值
    #         'price': 222,
    #         'date': '2011-11-11'
    #     }
    # )
    # print(obj1, created_status)  # 水浒12--222 True 新创建的数据,created_status为true,返回的第一个值为新创建的model对象

    # obj1, created_status = models.Book.objects.update_or_create(
    #     price=100,  # 查询参数,如果查询的数据为多条,会报错,因为内部调用的是get方法
    #     #错误信息: get() returned more than one Book -- it returned 2!
    #     defaults={  # 需要更新的值
    #         'name': 'xxx',
    #         'date': '2012-2-2'
    #     }
    # )
    # print(obj1, created_status)  # 水浒12--222 True 新创建的数据,created_status为true,返回的第一个值为新创建的model对象
    
    return HttpResponse('ok')
```



查询

```python
<1> all():                  查询所有结果，结果是queryset类型
  
<2> filter(**kwargs):       它包含了与所给筛选条件相匹配的对象，结果也是queryset类型 Book.objects.filter(title='linux',price=100) #里面的多个条件用逗号分开，并且这几个条件必须都成立，是and的关系，or关系的我们后面再学，直接在这里写是搞不定or的
  
<3> get(**kwargs):          返回与所给筛选条件相匹配的对象，不是queryset类型，是行记录对象，返回结果有且只有一个，
                            如果符合筛选条件的对象超过一个或者没有都会抛出错误。捕获异常try。  Book.objects.get(id=1)
  
<4> exclude(**kwargs):      排除的意思，它包含了与所给筛选条件不匹配的对象，没有不等于的操作昂，用这个exclude，返回值是queryset类型 Book.objects.exclude(id=6)，返回id不等于6的所有的对象，或者在queryset基础上调用，Book.objects.all().exclude(id=6)
 　　　　　　　　　　　　　　　　
<5> order_by(*field):       queryset类型的数据来调用，对查询结果排序,默认是按照id来升序排列的，返回值还是queryset类型
　　　　　　　　　　　　　　　　  models.Book.objects.all().order_by('price','id') #直接写price，默认是按照price升序排列，按照字段降序排列，就写个负号就行了order_by('-price'),order_by('price','id')是多条件排序，按照price进行升序，price相同的数据，按照id进行升序
        
<6> reverse():              queryset类型的数据来调用，对查询结果反向排序，返回值还是queryset类型
  
<7> count():                queryset类型的数据来调用，返回数据库中匹配查询(QuerySet)的对象数量。
  
<8> first():                queryset类型的数据来调用，返回第一条记录 Book.objects.all()[0] = Book.objects.all().first()，得到的都是model对象，不是queryset
  
<9> last():                queryset类型的数据来调用，返回最后一条记录
  
<10> exists():              queryset类型的数据来调用，如果QuerySet包含数据，就返回True，否则返回False
　　　　　　　　　　　　　　     空的queryset类型数据也有布尔值True和False，但是一般不用它来判断数据库里面是不是有数据，如果有大量的数据，你用它来判断，那么就需要查询出所有的数据，效率太差了，用count或者exits
　　　　　　　　　　　　　　　　 例：all_books = models.Book.objects.all().exists() #翻译成的sql是SELECT (1) AS `a` FROM `app01_book` LIMIT 1，就是通过limit 1，取一条来看看是不是有数据

<11> values(*field):        用的比较多，queryset类型的数据来调用，返回一个ValueQuerySet——一个特殊的QuerySet，运行后得到的并不是一系列
                            model的实例化对象，而是一个可迭代的字典序列,只要是返回的queryset类型，就可以继续链式调用queryset类型的其他的查找方法，其他方法也是一样的。
<12> values_list(*field):   它与values()非常相似，它返回的是一个元组序列，values返回的是一个字典序列
 
<13> distinct():            values和values_list得到的queryset类型的数据来调用，从返回结果中剔除重复纪录
```



作业:

```
1.点击添加按钮,跳转到添加页面,用户输入内容,然后点击提交按钮,将用户输入的数据保存到数据库,并且自动跳转到信息展示页面
2.点击删除按钮,从数据库中删除对应的数据,并且自动跳转到信息展示页面
3.点击编辑按钮,跳转到 一个新的页面,这个页面的形式和添加页面是一样的,但是里面有有默认值,然后用户点击提交,将用户提交的数据更新到对应的记录里面,并且自动跳转回信息展示页面

```











