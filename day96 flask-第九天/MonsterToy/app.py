from flask import Flask

from serv.content import content_bp
from serv.devices import devices_bp
from serv.user import user_bp

app = Flask(__name__)
app.config["DEBUG"] = True

app.register_blueprint(content_bp)
app.register_blueprint(user_bp)
app.register_blueprint(devices_bp)


if __name__ == '__main__':
    app.run("0.0.0.0",9527)