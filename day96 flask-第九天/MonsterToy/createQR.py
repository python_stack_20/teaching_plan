import os

import requests
from setting import LT_URL, MDB, QRCODE_PATH
#
# res = requests.get(""%("xiaogegexiaojiejie"))
#
# with open("aaa.jpg","wb") as f:
#     f.write(res.content)


from uuid import uuid4
import time, hashlib

device_list = []

for i in range(5):
    qr_str = hashlib.md5(f"{uuid4()}{time.time()}{uuid4()}".encode("utf8")).hexdigest()
    print(qr_str)
    device_info = {"device_key": qr_str}
    device_list.append(device_info)

    res = requests.get(LT_URL % (qr_str))

    qrfile_path = os.path.join(QRCODE_PATH, f"{qr_str}.jpg")

    with open(qrfile_path, "wb") as f:
        f.write(res.content)

MDB.Devices.insert_many(device_list)
