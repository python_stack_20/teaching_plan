from bson import ObjectId
from flask import Blueprint, request, jsonify

from setting import MDB, RET

devices_bp = Blueprint("devices_bp", __name__)


@devices_bp.route("/scan_qr", methods=["POST"])
def scan_qr():
    # 扫码绑定
    # 1.扫描成功,没有绑定 开启绑定
    # 2.扫码失败,未授权
    # 3.扫码成功,已经绑定  ?添加好友
    device_key = request.form.to_dict()
    device = MDB.Devices.find_one(device_key)
    if device:
        # 1.扫描成功,没有绑定 开启绑定
        RET["CODE"] = 0
        RET["MSG"] = "识别玩具成功"
        RET["DATA"] = device_key

    else:
        # 2.扫码失败, 未授权 授权库没有此条码
        RET["CODE"] = 1
        RET["MSG"] = "请不要瞎JB乱扫"
        RET["DATA"] = {}

    # // 3.蜜汁逻辑
    # 二维码扫描成功, 但设备已经进行绑定
    # {
    #     "code": 2,
    #     "msg": "设备已经进行绑定",
    #     "data":
    #         {
    #             "toy_id": toy_id
    #         }
    # }

    return jsonify(RET)




@devices_bp.route("/bind_toy", methods=["POST"])
def bind_toy():
    toy_info = request.form.to_dict()
    user_id = toy_info.pop("user_id")

    user_info = MDB.Users.find_one({"_id": ObjectId(user_id)})  # 查询user_info

    # 创建一个Chats
    chat_id = MDB.Chats.insert_one({"user_list": [], "chat_list": []})

    # 1.创建toy
    toy_info["avatar"] = "toy.jpg"
    toy_info["friend_list"] = []
    toy_info["bind_user"] = user_id
    # toy_info[bind_user]? 这个值怎么获得?
    # toy_id = MDB.Toys.insert_one(toy_info) 暂时不创建toy
    # 2.toy有了app的绑定对象, app的绑定toy对象是谁呢?
    # 在 Users 数据中的 bind_toys 列表 加入 toy 绑定对象的_id字符串
    # MDB.Users.update_one({}, {"$push": {"bind_toys": str(toy_id.inserted_id)}}) 暂时不创建
    # 3.将toy和app 交换名片
    # 建立一个移动端好友关系是为了 即使通讯 IM 基于通讯录的
    # 给玩具增加第一个好友 app
    toy_add_user = {
                       "friend_id": user_id,  # app id
                       "friend_nick": user_info.get("nickname"),  # user 昵称
                       "friend_remark": toy_info.pop("remark"),  # remark在哪里呢?
                       "friend_avatar": user_info.get("avatar"),  # user有头像吗?
                       "friend_chat": str(chat_id.inserted_id),
                       # 私聊窗口ID 聊天数据表对应值 ---- Q宠大乐斗 战斗记录
                       # """
                       # {
                       #     player_list:["P1","P2"],
                       #     Battle_list:[
                       #         {
                       #             acter:"P1",
                       #             defer:"P2",
                       #             info:"1232132131232131231",
                       #             ctime:2019.7.22
                       #         }
                       #     ]
                       # }
                       # """
                       # 那么问题来了,这个位置要存储一个Id ,是Chats数据表中的一条数据的Id
                       # 1.先创建 Chats 数据 可以获得ObjectId这个Id转化为字符串可以存储在 friend_chat
                       # 弊端 - player_list 中缺少 Toy_id 因为Toy还未创建完成

                       # 2.先空着 friend_chat 优先创建 Toy 获得 Toy_id
                       # 当user_id 遇到 Toy_id 就可以去创建 Chats ,player_list=[user_id,Toy_id]
                       # 再回到 Toys中 修改 friend_id == user_id 这条数据的 friend_chat
                       "friend_type": "app"  # 好友的用户类型 app / toy
                   },

    toy_info["friend_list"].append(toy_add_user)
    toy_id = MDB.Toys.insert_one(toy_info)

    # 一个人无法决定两人的关系 app 也要同时 增加toy为好友

    user_add_toy = {
        "friend_id": toy_id.inserted_id,  # toy_id str
        "friend_nick": toy_info.get("baby_name"),  # baby_name
        "friend_remark": toy_info.get("toy_name"),  # toy_name
        "friend_avatar": "toy.jpg",  # 阿凡达
        "friend_chat": str(chat_id.inserted_id),  # chat_id
        "friend_type": "toy"  # 好友的类型 toy
    }

    user_info["bind_toys"].append(str(toy_id.inserted_id))
    user_info["friend_list"].append(user_add_toy)

    # 修改 Users 的全部数据
    MDB.Users.update_one({"_id": ObjectId(user_id)}, {"$set": user_info})

    # Chats 数据也会变化 user_list player_list 将 toy_id  和 user_id 加入
    MDB.Chats.update_one({"_id":chat_id.inserted_id}, {"$set": {"user_list": [user_id,str(toy_id.inserted_id)]}})

    RET["CODE"] = 0
    RET["MSG"] = "绑定完成"
    RET["DATA"] = {}

    return jsonify(RET)


@devices_bp.route("/toy_list", methods=["POST"])
def toy_list():
    bind_user = request.form.get("_id")
    toyl = list(MDB.Toys.find({"bind_user":bind_user}))

    for toy in toyl:
        toy["_id"] = str(toy.get("_id"))

    RET["CODE"] = 0
    RET["MSG"] = "获取Toy列表"
    RET["DATA"] = toyl

    return jsonify(RET)