import os

from flask import Blueprint, jsonify, send_file
from setting import MDB,COVER_PATH,MUSIC_PATH

content_bp = Blueprint("content_bp",__name__)


@content_bp.route("/content_list",methods=["POST"])
def content_list():
    content = list(MDB.Content.find({}))

    for index,item in enumerate(content):
        content[index]["_id"] = str(item.get("_id"))

    return jsonify(content)


@content_bp.route("/get_cover/<filename>",methods=["GET"])
def get_cover(filename):

    cover_path = os.path.join(COVER_PATH,filename)

    return send_file(cover_path)


@content_bp.route("/get_music/<filename>",methods=["GET"])
def get_music(filename):

    music_path = os.path.join(MUSIC_PATH,filename)

    return send_file(music_path)