from bson import ObjectId
from flask import Blueprint, request, jsonify
from setting import MDB,RET

devices_bp = Blueprint("devices_bp",__name__)


@devices_bp.route("/scan_qr",methods=["POST"])
def scan_qr():
    device = request.form.to_dict()
    device_info = MDB.Devices.find_one(device)
    if device_info:
        # 扫描二维码成功 - 数据库中存在的DeviceKey
        RET["CODE"] = 0
        RET["MSG"] = "二维码扫描成功"
        RET["DATA"] = device
    else:
        # 扫描二维码失败 - 数据库中未存在DeviceKey
        RET["CODE"] = 1
        RET["MSG"] = "请不要乱扫"
        RET["DATA"] = {}

    # // 3.
    # 二维码扫描成功, 但设备已经进行绑定
    # {
    #     "code": 2,
    #     "msg": "设备已经进行绑定",
    #     "data":
    #         {
    #             "toy_id": toy_id
    #         }
    # }


    return jsonify(RET)



@devices_bp.route("/bind_toy",methods=["POST"])
def bind_toy():
    """

    :return:
    """
    toy_info = request.form.to_dict()
    toy_info["avatar"] = "toy.jpg"
    user_id = toy_info.pop("user_id")
    toy_info["bind_user"] = user_id
    toy_info["friend_list"] = []

    user_info = MDB.Users.find_one({"_id":ObjectId(user_id)})

    """
    { // 通讯录信息
			"friend_id" : "5ca17c7aea512d26281bcb8d", // 好友id
			"friend_nick" : "背背", // 好友的昵称
			"friend_remark" : "臭屎蛋儿", // 好友备注
			"friend_avatar" : "toy.jpg", // 好友头像
			"friend_chat" : "5ca17c7aea512d26281bcb8c", // 私聊窗口ID 聊天数据表对应值
			"friend_type" : "toy" // 好友类型
		},
    """

    # 这里创建聊天窗口 ???????????

    t_add_a  = {
        "friend_id":toy_info.get("bind_user"), # 用户的ID
        "friend_nick":user_info.get("nickname"),
        "friend_remark":"替换成对他/她的称呼", # 好友备注
        "friend_avatar":"", # 好友头像
        "friend_chat" : "string ObjectId", # 创建一个 Chats 数据表 插入一条数据 inserted_id ObjectId
        # 提前创建好 聊天窗口chat记录 可以获得这个Id 但是 user_list中 无法获取到 toy的id
        # 方法一 : 要么提前创建好 空user_list,获得toy_id之后 补全 user_list
        # 方法二 : 先创建 toy_id ,[user_id,toy_id] 创建 Chats
        # 再将 friend_id == user 的数据 friend_chat 更改为 chat_id
        "friend_type":"app"
    }

    toy_info["friend_list"].append(t_add_a)

    toy_id = MDB.Toys.insert_one(toy_info)

    # 还是在这里创建聊天窗口 ???????????

    a_add_t = {
        "friend_id": toy_id.inserted_id, # 字符串儿
        "friend_nick": "baby_name",
        "friend_remark": "toy_name",
        "friend_avatar": "",
        "friend_chat": "与t_add_a ID相同",
        "friend_type": "toy"
    }

    # user_info["bind_toys"].append(str(toy_id.inserted_id))
    # user_info["friend_list"].append(a_add_t)


    # MDB.Users.update_one({"_id":ObjectId(toy_info["bind_user"])},{"$push":{"bind_toys":str(toy_id.inserted_id)}})

    # MDB.Users.update_one({"_id":ObjectId(toy_info["bind_user"])},{"$set":user_info})

    RET["CODE"] = 0
    RET["MSG"] = "绑定成功"
    RET["DATA"] = {}

    return jsonify(RET)


@devices_bp.route("/toy_list",methods=["POST"])
def toy_list():
    """
    1.获取 _id 来查询 User 数据
    2.bind_toys = [toy_id]
    3.toy_id 查询玩具

    1.获取 _id
    2.通过 玩具的 bind_user 查询用户的绑定玩具
    :return:
    """
    user_id = request.form.get("_id")
    toyl = list(MDB.Toys.find({"bind_user":user_id}))
    print(toyl)

    for index,item in enumerate(toyl):
        toyl[index]["_id"] = str(item.get("_id"))

    RET["CODE"] = 0
    RET["MSG"] = "获取Toy列表"
    RET["DATA"] = toyl

    return jsonify(RET)