import os

from bson import ObjectId
from flask import Blueprint, jsonify, send_file,request
from setting import MDB,COVER_PATH,MUSIC_PATH,RET

user_bp = Blueprint("user_bp",__name__)


@user_bp.route("/reg",methods=["POST"])
def reg():
    user_info = request.form.to_dict()

    user_info["avatar"] = "baba.jpg" if user_info.get("gender") == "2" else "mama.jpg"
    user_info["bind_toys"] = []
    user_info["friend_list"] = []

    MDB.Users.insert_one(user_info)
    return jsonify({"CODE":0,"MSG":"注册成功","DATA":{}})


@user_bp.route("/login",methods=["POST"])
def login():
    user_info = request.form.to_dict()
    user_info_dict = MDB.Users.find_one(user_info)

    user_info_dict["_id"] = str(user_info_dict.get("_id"))

    RET["CODE"] = 0
    RET["MSG"] = f"欢迎{user_info_dict.get('nickname')}登录"
    RET["DATA"] = user_info_dict

    return jsonify(RET)

@user_bp.route("/auto_login",methods=["POST"])
def auto_login():
    user_info = request.form.to_dict()
    user_info["_id"] = ObjectId(user_info.get("_id"))
    user_info_dict = MDB.Users.find_one(user_info)

    user_info_dict["_id"] = str(user_info_dict.get("_id"))

    RET["CODE"] = 0
    RET["MSG"] = f"欢迎{user_info_dict.get('nickname')}登录"
    RET["DATA"] = user_info_dict

    return jsonify(RET)
