from django.db import models

# Create your models here.

class Book(models.Model):

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=32,unique=True)
    price = models.IntegerField(null=True,blank=True,default=11,db_index=True)

    sex = (
        (0,'男'),(1,'女')
    )
    sex = models.IntegerField(choices=sex)

    date = models.DateField(null=True,auto_now=True)
    date2 = models.DateField(null=True,auto_now_add=True)









