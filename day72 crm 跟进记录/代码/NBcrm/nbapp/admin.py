from django.contrib import admin

# Register your models here.
from nbapp import models


admin.site.register(models.UserInfo)
admin.site.register(models.Customer)
admin.site.register(models.Campuses)
admin.site.register(models.ClassList)




