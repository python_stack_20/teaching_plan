# 昨日内容回顾

## form

1.生成HTML标签

2.校验数据

3.保留用户输入的内容



用法:

```python
import re
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
def mobile_validate(value):
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value):
        raise ValidationError('手机号码格式错误')  

class Myform(forms.Form):
	title = forms.Charfield(
		label='书名',
		max_length=16,
		min_length=12,
		initial = 'xx',
		help_text = 'aaaa',
		required=False,
		error_messages={'max_length':'xxxx'.....},
		widget = forms.widgets.TextInput(attrs={'type':'date'}),
		widget = forms.widgets.PasswordInput(attrs={'class':'...'},render_value=True),
		validators = [RegexValidator(r'^a','错误信息'),mobile_validate,]
		
	)
	price=forms.IntergerField...
	
	局部钩子:
	def clean_title(self):
		val = self.cleaned_data.get('title')
		#正确的话:return val
		raise ValidationError('错误')  
    全局钩子:
    def clean(self):
    	self.cleaned_data
    	return self.cleaned_data
    	raise ValidationError('错误')  --- 加到全局的错误里面了, self.errors  form_obj= Myform()
    	self.add_error('字段名','错误信息')
	
	def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs) 
    	for field in self.fields:
            self.fields[field].widgets.attrs.update({
                'class':'form-control',
            })
       
    #单选下拉框和单选radio
    方式1
	publish = forms.ChoiceField(
    	choices=(
        	(1,'xx'),
            (2,'xx'),
        ),
        widget = forms.widgets.Select(),
        widget = forms.widgets.RaidoSelect(),
    )
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs) 
		self.fields['publish'].choices = models.Publish.objects.values_list('id','name')
    方式2
    publish = forms.ModelChoiceField(
    	queryset = models.Publish.objects.all(),  #objects对象  models--def __str__(self):return self.name
        widget = forms.widgets.Select(),
        widget = forms.widgets.RaidoSelect(),
    )
    
    #多选下拉框和多选checkbox
     Authors = forms.ModelMultipleChoiceField(
     	queryset = models.Author.objects.all(),
        widget = forms.widgets.SelectMultiple(),
        widget = forms.widgets.CheckboxSelectMultiple(),
     )
     
    
    #单选的checkbox
    class TestForm2(forms.Form):
        keep = forms.ChoiceField(
            choices=(
                ('True',1),
                ('False',0),
            ),

            label="是否7天内自动登录",
            initial="1",
            widget=forms.widgets.CheckboxInput(), 
        )
	选中:'True'   #form只是帮我们做校验,校验选择内容的时候,就是看在没在我们的choices里面,里面有这个值,表示合法,没有就不合法
    没选中:'False'
    ---保存到数据库里面  keep:'True'
    if keep == 'True':
        session 设置有效期7天
    else:
        pass
```









