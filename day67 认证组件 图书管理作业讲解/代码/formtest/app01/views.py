from django.shortcuts import render,HttpResponse,redirect
from django import forms
# Create your views here.
from app01 import models

class MyForm(forms.Form):
    title = forms.CharField(
        max_length=32,min_length=2,
        help_text='这里是输入用户名的地方,别忘了不能小于2位!',
        error_messages={
            "min_length":'长度不能小于2',
            "required":'该字段不能为空!',
            "max_length":'字段过长,不能超过32位!'
        },
        label='书名',
        widget=forms.widgets.TextInput(attrs={'placeholder':'这里是输入用户名的地方,别忘了不能小于2位!'})
    )
    price=forms.IntegerField(
        widget=forms.widgets.NumberInput()
    )
    publishDate = forms.DateField(
        widget=forms.widgets.DateInput(attrs={'type':'date'})
    )

    publish = forms.ModelChoiceField(
        queryset=models.Publish.objects.all(),
        widget=forms.widgets.Select()
    )

    authors = forms.ModelMultipleChoiceField(
        queryset=models.Author.objects.all(),
        widget = forms.widgets.SelectMultiple()
    )
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })

def create_book(request):
    if request.method == 'GET':
        form_obj = MyForm()
        return render(request, 'index.html', {'form_obj': form_obj})
    else:
        data= request.POST
        form_obj = MyForm(data)
        if form_obj.is_valid(): #验证每个字段传过来的数据是不是正确的
            data = form_obj.cleaned_data
            author_data = data.pop('authors')
            book_obj = models.Book.objects.create(**data)
            book_obj.authors.add(*author_data)
            return redirect('show')
        else:
            print(form_obj.errors)
        return render(request, 'index.html', {'form_obj': form_obj})


def show(request):

    all_books = models.Book.objects.all()
    # all_books = models.Book.objects.all().first()
    # all_books.authors.all() #多对多
    # all_books.publish.name #一对多
    return render(request,'show.html',{'all_books':all_books})

def edit_book(request,n):
    book_obj = models.Book.objects.filter(pk=n)

    if request.method == 'GET':
        book_obj = book_obj.first()
        all_publish = models.Publish.objects.all()
        all_authors = models.Author.objects.all()
        return render(request,'edit_book.html',{'n':n,'book_obj':book_obj,'all_publish':all_publish,'all_authors':all_authors})

    else:
        author_data = request.POST.getlist('authors') # 取多选数据的时候用getlist方法
        print('author_data',author_data) #author_data ['2', '3']
        data = request.POST.dict()
        print(data)
        del data['csrfmiddlewaretoken']
        data.pop('authors')  #authors:[1,2]

        book_obj.update(**data)

        book_obj[0].authors.set(author_data)
        return redirect('show')

def delete_book(request,n):
    models.Book.objects.filter(pk=n).delete()
    return redirect('show')
from django.http import JsonResponse

def ajax_delete_book(request,n):
    data = {'status':0}
    try:
        data['status'] = 1
        models.Book.objects.filter(pk=n).delete()
    except Exception:
        data['status'] = 2
    return JsonResponse(data)




