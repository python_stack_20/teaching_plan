from django.shortcuts import render,redirect,HttpResponse
from django.contrib.auth.models import User
# Create your views here.
from app01 import models
from django.contrib import auth

def register(request):
    if request.method == 'GET':
        return render(request,'register.html')
    else:
        username = request.POST.get('user')
        password = request.POST.get('pwd')
        idcard = request.POST.get('id_card')

        print(username,password)
        if models.UserInfo.objects.filter(username=username,).exists():
            return HttpResponse('用户名已经存在')
        models.UserInfo.objects.create_user(username=username,password=password,id_card=idcard) #创建普通用户
        # User.objects.create_superuser(username=username,password=password) #创建超级用户
        return redirect('login')


def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('user')
        password = request.POST.get('pwd')

        # obj = User.objects.filter(username=username,password=password).first()
        user_obj = auth.authenticate(username=username,password=password) #查不到内容返回None,查到了返回一个model对象

        if user_obj:
            auth.login(request,user_obj)
            #1. 干了和session一样的事情
            #2. request.user = user_obj
            return redirect('home')
        else:
            return redirect('login')

def home(request):
    # print(request.user) #AnonymousUser wuchao
    # print(request.user.id) #None 1
    # print(request.user.username) # '' wuchao
    # print(request.user.is_active) #False True
    if request.user.is_authenticated(): #判断用户是否已经登录了
        return render(request,'home.html')
    else:
        return redirect('login')

def logout(request):
    auth.logout(request)  #注销:清除了session表的记录和cookie里面的东西
    return redirect('login')


def set_password(request):
    if request.method == 'GET':
        return render(request,'set_password.html')

    else:
        old_pwd = request.POST.get('old_password')
        new_pwd = request.POST.get('new_password')
        r_new_pwd = request.POST.get('r_new_password')

        if request.user.check_password(old_pwd): #校验旧密码的方法,成功返回True,不成功返回False
            if new_pwd != r_new_pwd:
                return HttpResponse('两次输入不一致!!')
            else:
                request.user.set_password(new_pwd) #修改密码
                request.user.save() #保存
                return redirect('login')
        else:
            return HttpResponse('旧密码输入有误,你到底是谁!!!不要搞事情!!!')


from app01 import models

#两种更新
# models.Book.objects.filter(title='金瓶梅').update()
# #date1 = models.DateField(auto_now=True)
# #date2 = models.DateField(auto_now_add=True)
#
# book_obj = models.Book.objects.filter(title='金瓶梅').first()
# book_obj.title = 'python'
# book_obj.save()
# #date = models.DateField(auto_now=True)字段数据才会自动更新
# models.Book.objects.filter(title='金瓶梅').update(
#     date1 = datetime.datetime.now(),
#     date2 = datetime.datetime.now(),
# )







