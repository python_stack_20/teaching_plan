"""NBcrm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from nbapp import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #首页
    url(r'^index/', views.index,name='index'),

    #注册
    url(r'^register/', views.register, name='register'),

    #登录
    url(r'^login/', views.login, name='login'),

    #验证码
    url(r'^get_valid_img/', views.get_valid_img, name='get_valid_img'),

    # 注销
    url(r'^logout/', views.logout, name='logout'),
    # url(r'^shopping/', views.shopping, name='shopping'),
    # 公户信息展示
    url(r'^customers/list/', views.CustomerView.as_view(), name='customers'),

    # 私户信息展示
    url(r'^mycustomers/', views.CustomerView.as_view(), name='mycustomers'),
    # url(r'^mycustomers/', views.mycustomers, name='mycustomers'),
    #测试分页
    url(r'^test/', views.test, name='test'),

    #添加公共客户信息路径
    url(r'^customers/add/', views.AddCustomer.as_view(), name='addcustomer'),
    #修改公共客户信息路径
    url(r'^customers/edit/(\d+)/', views.EditCustomer.as_view(), name='editcustomer'),
    #删除公共客户信息路径
    url(r'^customers/delete/(\d+)/', views.DeleteCustomer.as_view(), name='deletecustomer'),

    #跟进记录
    url(r'^consultrecord/$', views.ConsultRecordView.as_view(), name='consultrecord'),
    url(r'^consultrecord/(\d+)/$', views.ConsultRecordView.as_view(), name='singleconsultrecord'),

    #添加跟进记录
    url(r'^consultrecord/add/', views.AddConsultRecordView.as_view(), name='addconsultrecord'),
    url(r'^consultrecord/edit/(\d+)/', views.AddConsultRecordView.as_view(), name='editconsultrecord'),

    #班级学习记录
    url(r'^class_record/', views.ClassRecordView.as_view(), name='class_record'),
    #学详路径
    url(r'^study_decord/(\d+)/', views.StudyRecordDeialView.as_view(), name='study_decord'),



]



