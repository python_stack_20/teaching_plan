redis学习


mysql 关系型数据库
非关系型数据库 redis    mongodb
nosql  不仅仅是sql

数据持久化,可以将数据存储到磁盘上,以文件形式存储

redis是内存性的数据库,读写是非常快的
缺点是:断电释放内存数据,redis数据丢失,redis进程挂掉,数据丢失,redis提供了持久化机制

redis的安装方式
1.yum  install  redis -y
rpm

源码编译(在选择编译的时候,注意删掉之前的yum安装的)
1.yum remove redis -y 

2.下载redis的源代码包
wget http://download.redis.io/releases/redis-4.0.10.tar.gz

3.解压缩源码包
编译三部曲:
#(此redis压缩包,已经提供好了makefile,只需要执行,编译的第二,第三曲)

2.执行gcc的make指令,执行makefile文件
make 
3.开始安装
make install  

4.会安装在当前的源码包中的src目录,且帮你配置好了PATH变量
通过redis-加上tab补全查看所有命令

redis-benchmark  redis-check-rdb  redis-sentinel   
redis-check-aof  redis-cli        redis-server  

5.制定一个安装可靠的redis数据库,如下功能通过配置文件定义
1.更改端口
2.设置密码
3.开启redis的安全启动模式
默认直接输入redis-server可以启动服务端,默认端口6379,且没有密码
redis-cli登录


redis.conf 内容如下,有多少参数,就有多少功能,

bind 192.168.16.142    #绑定redis启动的地址
protected-mode yes		#开启redis的安全模式,必须输入密码才可以远程登录
port 6380			 #指定redis的端口  
daemonize no			#让redis以守护进程方式在后台运行,不占用窗口
pidfile /var/run/redis_6379.pid   #记录redis的进程id号的文件
loglevel notice    		#日志运行等级 .严重级别,警告级别,debug调试界别.....logging
requirepass haohaio     #设置redis的密码,是 haohaio


指定配置文件的启动方式
redis-server  s20redis.conf  

#此时登录redis必须加上参数了,并且登录了之后,必须输入密码才可以使用 
redis-cli -p 6380 -h 192.168.16.142


学习redis的数据类型使用

1.常用redis的公共命令
keys *         查看所有key
type key      查看key类型
expire key seconds    过期时间
ttl key     查看key过期剩余时间        -2表示key已经不存在了
persist     取消key的过期时间   -1表示key存在，没有过期时间

exists key     判断key存在    存在返回1    否则0
del keys     删除key    可以删除多个
dbsize         计算key的数量




2.学习string类型的操作
#通过set设置的就是string类型的key -value

set 　　设置key
get   获取key
append  追加string
mset   设置多个键值对
mget   获取多个键值对
del  删除key
incr  递增+1
decr  递减-1

list类型,双向队列





dic1={
	'k1':{"k2":"v2"}
}



dic1[k1][k2]

#redis的发布订阅
#QQ群  
详细命令看博客


#redis的持久化机制
分为aof和rdb两种,具体看博客



#在不重启redis的情况下,切换rdb中的数据,到aof中的操作
环境准备
1.配置一个rdb的redis服务端
	s20rdb.conf内容如下
		daemonize yes
		port 6379
		logfile /data/6379/redis.log
		dir /data/6379
		dbfilename  dbmp.rdb 
		bind  127.0.0.1
		save 900 1       
		save 300 10
		save 60  10000

2.基于这个文件启动redis,支持rdb的数据库
redis-server s20rdb.conf 

3.登录redis设置key,然后手动触发save,生成rdb数据文件

4.通过登录redis,两条命令,切换为aof持久化方式
127.0.0.1:6379> 
127.0.0.1:6379> CONFIG set appendonly yes 
OK
127.0.0.1:6379> CONFIG SET save ""
OK

5.第四部的操作,仅仅是临时生效,还得修改配置文件,保证下次重启,也得用aof进行持久化


save 900 1：表示900 秒内如果至少有 1 个 key 的值变化，则保存
save 300 10：表示300 秒内如果至少有 10 个 key 的值变化，则保存
save 60 10000：表示60 秒内如果至少有 10000 个 key 的值变化，则保存

redis是单线程的,由c编写 

       
#redis的主从复制
1.redis和mysql都是支持多实例功能,基于配置文件区分,就是一个数据库单独的实例

环境准备,一个6379的redis(master),和一个6380的redis(slave)

分别准备2个配置文件,内容如下
redis-6379.conf
port 6379
daemonize yes
pidfile /data/6379/redis.pid
loglevel notice
logfile "/data/6379/redis.log"
dbfilename dump.rdb
dir /data/6379
protected-mode no


redis-6380.conf (从)

port 6380
daemonize yes
pidfile /data/6380/redis.pid
loglevel notice
logfile "/data/6380/redis.log"
dbfilename dump.rdb
dir /data/6380
protected-mode no
slaveof   127.0.0.1  6379

2.分别启动2个redis实例
 1061  redis-server redis-6379.conf 
 1062  redis-server redis-6380.conf 

3.登录数据库,查看两人之间的关系

登录6379数据库,输入如下命令
127.0.0.1:6379> info  replication


6380数据库查看如下关系
[root@s20 smredis]# redis-cli -p 6380  info replication

4.数据读写查看
在6379中是可以读写的
redis-cli -p 6379  set name5  haoxiangxiake



redis-cli -p 6380 get name5


5.主从复制故障恢复
从库挂了无所谓
主库挂了得立即恢复主库,或者将从库切换为主库,继续工作

	-.实验步骤
	1.分别启动 6379  6380 6381 三个数据库实例,建立好主从关系
	
	2.杀死6379主库,此时6380 6381 群龙无首
	
	3.选择让6381为新的主库,就要去除6381的从的身份
	redis-cli -p 6381 slaveof  no one 
	#查看此时6381的身份
	redis-cli -p 6381 info replication

	4.此时将6380的主人改为6381
	redis-cli -p 6380 slaveof 127.0.0.1 6381
	
6.redis的哨兵,自动的主从故障切换
配置步骤

1.环境准备,准备3个redis数据库实例,分别是 6379  6380  6381  
redis-6379.conf 
port 6379
daemonize yes
logfile "6379.log"
dbfilename "dump-6379.rdb"
dir "/var/redis/data/6379"
	
	
redis-6380.conf 
port 6380
daemonize yes
logfile "6380.log"
dbfilename "dump-6380.rdb"
dir "/var/redis/data/6380" 
slaveof 127.0.0.1 6379    

redis-6381.conf 
port 6381
daemonize yes
logfile "6380.log"
dbfilename "dump-6380.rdb"
dir "/var/redis/data/6381" 
slaveof 127.0.0.1 6379    

	
2.分别启动三个redis数据库实例 
 1106  redis-server redis-6379.conf 
 1107  redis-server redis-6380.conf 
 1108  redis-server redis-6381.conf 

3.准备三个redis-sentinel哨兵的配置文件

redis-sentinel-26379.conf
port 26379  
dir /var/redis/data/26379
logfile "26379.log"

// 当前Sentinel节点监控 127.0.0.1:6379 这个主节点
// 2代表判断主节点失败至少需要2个Sentinel节点节点同意
// mymaster是主节点的别名
sentinel monitor s20master 127.0.0.1   6379  2

//每个Sentinel节点都要定期PING命令来判断Redis数据节点和其余Sentinel节点是否可达，如果超过30000毫秒30s且没有回复，则判定不可达
sentinel down-after-milliseconds s20master 30000

//当Sentinel节点集合对主节点故障判定达成一致时，Sentinel领导者节点会做故障转移操作，选出新的主节点，
原来的从节点会向新的主节点发起复制操作，限制每次向新的主节点发起复制操作的从节点个数为1
sentinel parallel-syncs s20master 1

//故障转移超时时间为180000毫秒
sentinel failover-timeout s20master 180000
//让哨兵在后台运行
daemonize yes

	
#如下26380和26381的配置文件,仅仅是端口的不同,可以快速生成	
redis-sentinel-26380.conf
redis-sentinel-26381.conf

#生成命令如下
 1119  sed "s/26379/26380/g"  redis-26379.conf > redis-26380.conf
 1121  sed "s/26379/26381/g"  redis-26379.conf > redis-26381.conf



4.分别运行三个哨兵进程
(保证sentinel的配置正确,否则,你在启动报错后,配置文件的内容发发生变化,这是个坑!!!!)

#创建哨兵的数据文件夹
 1126  mkdir -p /var/redis/data/26380
 1127  mkdir -p /var/redis/data/26379
 1128  mkdir -p /var/redis/data/26381

 #分别启动三个哨兵
 
 1134  redis-sentinel redis-26379.conf 
 1137  redis-sentinel redis-26380.conf 
 1138  redis-sentinel redis-26381.conf 


 
 5.检查redis的哨兵状态
redis-cli -p 26379 info sentinel
#查看到如下参数,哨兵就正确了
master0:name=s20master,status=ok,address=127.0.0.1:6379,slaves=2,sentinels=3



6.杀死主库,检查主从状态,是否会切换













	
	







