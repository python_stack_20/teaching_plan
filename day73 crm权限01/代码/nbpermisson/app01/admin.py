from django.contrib import admin
from app01 import models
# Register your models here.

class PermissonAdmin(admin.ModelAdmin):
    list_display = ['pk','title','url']
    # ordering = ['-pk',]


class RoleAdmin(admin.ModelAdmin):
    list_display = ['pk','title']
    # ordering = ['-pk',]


admin.site.register(models.User)
admin.site.register(models.Role,RoleAdmin)
admin.site.register(models.Permisson,PermissonAdmin)


