from django.shortcuts import render,HttpResponse,redirect
from app01 import models
# Create your views here.

def login(request):
    if request.method == 'POST':
        user = request.POST.get('user')
        pwd = request.POST.get('pwd')
        user_obj = models.User.objects.filter(name=user,pwd=pwd).first()

        if user_obj:
            # 登录成功,保存登录状态
            request.session['user'] = user_obj.name

            # 查询权限,保存到session


            permissions = models.Permisson.objects.filter(role__user__name=user_obj.name).distinct()
            # print(permissions)
            permisson_list = [i.url for i in permissions]
            print(permisson_list)
            request.session['permisson_list'] = permisson_list

            return HttpResponse('登录成功')

        else:
            return redirect('login')
    else:
        return render(request,'login.html')

def order(request):

    return HttpResponse('order....')

def addorder(request):

    return HttpResponse('addorder....')

def editorder(request,pk):
    return HttpResponse('editorder....')

def deleteorder(request,pk):
    return HttpResponse('deleteorder....')


def customer(request):
    return HttpResponse('customer....')

def addcustomer(request):
    return HttpResponse('addcustomer....')

def editcustomer(request,pk):
    return HttpResponse('editcustomer....')

def deletecustomer(request,pk):
    return HttpResponse('deletecustomer....')




